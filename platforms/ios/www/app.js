Ext.Loader.setConfig({
    enabled: true
});

docDir = "";

reiniciando = false;

pedGravado = 0;

var contSql;

// PhoneGap is loaded and it is now safe to make calls PhoneGap methods
//

function onDeviceReady() {
    var body = document.getElementsByTagName('body')[0];
    body.style.minHeight=window.innerHeight + 'px';
    
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onFileSystemSuccess, fail);
    
        
    document.addEventListener("online", onOnline, false);
    
    document.addEventListener('touchmove', function(e) {
                              e.preventDefault();
                              }, { passive: false });
}

function onFileSystemSuccess(fileSystem) {    
    //alert(fileSystem.name);
    //alert(fileSystem.root.name);
    docDir = fileSystem.root.getDirectory("sayerlack", {create: true}, getDir,fail);
}

function getDir(dir){
    docDir = dir;
    //console.log("docDir: " + docDir.fullPath);
    
}

function fail(evt) {
    alert("Erro ao carregar FS");
    alert(evt.target.error.code);
}

function onOnline() {
    
    console.log('onOnline');
    
    var syncControl = App.app.getController('SyncOrder');

    syncControl.syncPedido();
}

function checkConnection() {
    var networkState = navigator.network.connection.type;
    
    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.NONE]    = 'No network connection';
    
    return states[networkState];
    
}

var custnum, custname, _myApp, db = openDatabase("ipadrev", "", "Revenda Sayerlack", 1024 * 1024 * 5);

Ext.application({
    name: 'App',
    models: [
        'Customers',
        'CustomerPaymentTerms',
        'Orders',
        'OrdersRemote',
        'OrderItems',
        'OrderItemsRemote',
        'ItemsGroups',
        'Support',
        'Settings',
        'SelectOptions',
        'PaymentTerms',
        'PriceTables',
        'Products',
        'Lines',
        'Colors',
        'Groups',
        'Correlatos',
        'OrderType',
        'Carriers'
    ],
    views: [
        'Login',
        'Main',
        'carriers.CarriersList',
        'customer.CustomersCard',
        'customer.CustomersList',
        'customer.CustomersForm',
        'order.OrdersCard',
        'order.OrdersList',
        'order.OrdersForm',
        'order.ItemsGroupsList',
        'order.AddItemForm',
        'settings.SettingsCard',
        'settings.SettingsForm',
        'support.SupportCard',
        'support.SupportList'
    ],    
    controllers: [
        'Main',
        'Customers',
        'Orders',
        'Sql',
        'SyncOrder',
        'Support'
    ],
    stores: [
        'Customers',
        'CustomersRemote',
        'CustomerPaymentTerms',
        'Orders',
        'OrderItems',
        'ItemsGroups',
        'Support',
        'Settings',
        'SiteList',
        'PaymentTerms',
        'PriceTables',
        'PriceTablesRemote',
        'Products',
        'Lines',
        'Colors',
        'Groups',
        'Correlatos',
        'OrderType',
        'Carriers'
    ],
    
    viewport: {        
        autoMaximize: true,
        scrollable: false,
        scroll: false,
        layout : {
            type: 'card',
            animation : 'cube',
            direction: 'left',
            duration: 200
        }
    },
    eventPublishers: {
        touchGesture: {
            moveThrottle: 5
        }
    },

    launch: function() {
                
        Ext.Msg.defaultAllowedConfig.showAnimation = false;
                
        //Ext.Viewport.add({ xtype: 'main' });
        Ext.create('App.view.Login', {fullscreen: true});

        document.addEventListener("deviceready", onDeviceReady, false);        
     
 
        _myApp = this;
    }
});

Ext.define('Ext.event.recognizer.DoubleTapOverride', {
           override: 'Ext.event.recognizer.DoubleTap',
           
           maxDuration: 0
});

Ext.define('Ext.data.proxy.SQLOverride', {
    override: 'Ext.data.proxy.SQL',
 
    getDatabaseObject: function() {
       
        contSql = App.app.getController('Sql');
           
        contSql.build();
           
        return db;

    }
});

Ext.define('Ext.Component', {
           override: 'Ext.Component',
           show: function (animation) {
           return this.callParent([false]);
           },
           hide: function (animation) {
           return this.callParent([false]);
           }
           });

if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength, padString) {
        targetLength = targetLength >> 0; //truncate if number, or convert non-number to 0;
        padString = String(typeof padString !== 'undefined' ? padString : ' ');
        if (this.length >= targetLength) {
            return String(this);
        } else {
            targetLength = targetLength - this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0, targetLength) + String(this);
        }
    };
}
