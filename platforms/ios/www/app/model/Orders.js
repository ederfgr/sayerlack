Ext.define('App.model.Orders', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
           // {name: 'id',  type: 'auto'},  
            {name: 'custnum',  type: 'string'},
            {name: 'custname', type: 'string'},
            {name: 'orderipad', type: 'string'},
            {name: 'ordernum',  type: 'string'},
            {name: 'ordercust', type: 'string'},
        	{name: 'createdate',  type: 'string'},
        	{name: 'siteid',  type: 'string'},
        	{name: 'deliverydate',  type: 'string'},
        	{name: 'discount',  type: 'string'},
        	{name: 'discountlist',  type: 'string'},
        	{name: 'ordertype',  type: 'string'},
            {name: 'paymentTerms', type: 'string'},
        	{name: 'remarks',  type: 'string'},
            {name: 'value',  type: 'string'},  
            {name: 'valueEMS',  type: 'string'},  
            {name: 'status',  type: 'string'},
            {name: 'statusEMS',  type: 'string'},
            {name: 'carrier', type: 'string'}
        ],
        idProperty: 'orderipad',
        listeners: {
            load: {
                fn: function(s,r,o) {

                    var customers = Ext.getStore('Customers');

                    s.each(function(record) {

                        var name = customers.getById(record.data.custnum);
                    })                    
                }
            }
        }
        //belongsTo: {model: 'App.model.Customers', associationKey: 'custnum', name: 'customers', foreignKey: 'code'},
        //hasMany: 'OrderItems'
    }  
});