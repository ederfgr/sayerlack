Ext.define('App.model.Carriers', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [          	
            {name: 'code',  type: 'string'},  
            {name: 'abrevName',  type: 'string'},
            {name: 'fullName', type: 'string'}
        ],
        idProperty: 'code'
    }  
});  