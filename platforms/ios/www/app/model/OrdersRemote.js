Ext.define('App.model.OrdersRemote', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'id',  type: 'auto'},  
            {name: 'custnum',  type: 'string'},
            {name: 'orderipad', type: 'string'},
            {name: 'ordernum',  type: 'string'},
            {name: 'ordercust', type: 'string'},
        	{name: 'createdate',  type: 'string'},
        	{name: 'siteid',  type: 'string'},
        	{name: 'deliverydate',  type: 'string'},
        	{name: 'discount',  type: 'string'},
        	{name: 'discountlist',  type: 'string'},
        	{name: 'ordertype',  type: 'string'},
        	{name: 'remarks',  type: 'string'},
            {name: 'value',  type: 'string'},
            {name: 'valueEMS',  type: 'string', originalValue: ''},
            {name: 'status',  type: 'string'},
            {name: 'statusEMS',  type: 'string'},
            {name: 'customers_id',  type: 'string'},
            {name: 'carrier', type: 'string'}
        ],
        idProperty: 'id',        
        hasMany: [{
          model: 'App.model.OrderItemsRemote',
          name: 'orderItems',          
          associationKey: 'orderitems'          
        }] 
    }  
});