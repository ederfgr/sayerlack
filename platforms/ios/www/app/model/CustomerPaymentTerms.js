Ext.define('App.model.CustomerPaymentTerms', {
    extend: 'Ext.data.Model',
    config: {
        fields: [
            {name: 'id', type: 'auto'},
            {name: 'custnum',  type: 'string'},
            {name: 'paymentTerm',  type: 'string'},
            {name: 'financialIndex', type: 'string'}
        ]
    }
});