Ext.define('App.model.OrderItems', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'id', type: 'string'},
            {name: 'orderipad',  type: 'string'},
        	{name: 'itemcode',  type: 'string'},
        	{name: 'description',  type: 'string'},
        	{name: 'un',  type: 'string'},
        	{name: 'quantity',  type: 'string'},
            {name: 'discount', type: 'string'},
        	{name: 'value',  type: 'string'},
            {name: 'valueOrig', type: 'string'}
        ]
        //idProperty: 'id'
        //belongsTo: 'Orders'
    }  
});