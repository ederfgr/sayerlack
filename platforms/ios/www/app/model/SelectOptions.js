Ext.define('App.model.SelectOptions', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'text',  type: 'string'},  
            {name: 'value',  type: 'string'}
        ],
        idProperty: 'value'  
    }  
});  