Ext.define('App.model.Colors', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'code',  type: 'string'},  
            {name: 'desc',  type: 'string'},
            {name: 'lineCode',  type: 'string'},
            {name: 'image', type: 'string'}
        ]
    }  
}); 