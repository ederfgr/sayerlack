Ext.define('App.model.Products', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'code',  type: 'string'},  
            {name: 'desc',  type: 'string'},
            {name: 'itemPai',  type: 'string'},
            {name: 'unidade',  type: 'string'},
            {name: 'loteMulti', type: 'string'},
            {name: 'grupo', type: 'string'}
        ],
        idProperty: 'code'
    }  
});  