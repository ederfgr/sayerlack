Ext.define('App.model.Settings', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'id', type: 'auto'},
            {name: 'urlws',  type: 'string'},
            {name: 'username',  type: 'string'},
            {name: 'password',  type: 'string'}, 
            {name: 'sitelist',  type: 'string'},
            {name: 'bloqIpad',  type: 'string'},
            {name: 'lastorder', type: 'string'},
            {name: 'minValue', type: 'string'},
            {name: 'minMsg', type: 'string'},
            {name: 'lastAcc', type: 'string'},
            {name: 'urlDownload',  type: 'string'},
            {name: 'diasSemPedido',  type: 'string'}                 
        ],
        idProperty: 'id'
    }
});