Ext.define('App.model.Correlatos', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'id', type: 'string'},
            {name: 'line',  type: 'string'},  
            {name: 'lineCorre',  type: 'string'}
        ]
    }  
}); 