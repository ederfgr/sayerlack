Ext.define('App.model.Lines', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'code',  type: 'string'},  
            {name: 'desc',  type: 'string'},
            {name: 'codeGroup',  type: 'string'},
            {name: 'image',  type: 'string'},
            {name: 'attr1',  type: 'string'},
            {name: 'attr2',  type: 'string'},
            {name: 'attr3',  type: 'string'},
            {name: 'attr4',  type: 'string'}
        ],
        idProperty: 'code'
    }  
});  