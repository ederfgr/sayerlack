Ext.define('App.model.PaymentTerms', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
        	{name: 'id'},
            {name: 'code',  type: 'string'},  
            {name: 'desc',  type: 'string'},
            {name: 'financialIndex', type: 'string'},
        ],
        //idProperty: 'code' 
    }
});