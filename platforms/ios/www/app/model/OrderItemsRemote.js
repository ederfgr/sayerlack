Ext.define('App.model.OrderItemsRemote', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [
            {name: 'id',  type: 'auto'},  
            {name: 'orderipad',  type: 'string'},
        	{name: 'itemcode',  type: 'string'},
        	{name: 'description',  type: 'string'},
        	{name: 'un',  type: 'string'},
        	{name: 'quantity',  type: 'string'},
        	{name: 'value',  type: 'string'}
        ],
        idProperty: 'id' 
    }  
});