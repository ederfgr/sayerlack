Ext.define('App.model.Groups', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
        	{name: 'id'},
            {name: 'code',  type: 'string'},  
            {name: 'descGroup',  type: 'string'},
            {name: 'codeLine', type: 'string'}
            
        ],
        //idProperty: 'code'
    }  
});  