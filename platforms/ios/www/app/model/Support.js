Ext.define('App.model.Support', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'filename',  type: 'string'},  
            {name: 'filetype',  type: 'string'},
            {name: 'description',  type: 'string'},
            {name: 'uptodate',  type: 'string'},
            {name: 'docDate', type: 'string'}
        ],
        idProperty: 'filename' 
    }  
});  