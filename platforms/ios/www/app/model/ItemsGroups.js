Ext.define('App.model.ItemsGroups', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
            {name: 'code',  type: 'string'},  
            {name: 'groupname',  type: 'string'},
           	{name: 'quantidade', type: 'number'},
           	{name: 'orderType', type: 'string'}
        ]  
    }  
});