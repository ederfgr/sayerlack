Ext.define('App.model.Customers', {  
    extend: 'Ext.data.Model',  
    requires: ['App.model.Orders'],
    config: {  
        fields: [  
            {name: 'id',  type: 'string'},
            {name: 'code',  type: 'string'},  
            {name: 'name',  type: 'string'},
            {name: 'cnpj',  type: 'string'},
            {name: 'abrev_name',  type: 'string'},
            {name: 'address',  type: 'string'},
            {name: 'city',  type: 'string'},
            {name: 'uf',  type: 'string'},
            {name: 'paymentTerm', type: 'string'},
            {name: 'ordertype', type: 'string'},
            {name: 'email', type: 'string'},
            {name: 'carrier', type: 'string'},
            {name: 'bairro', type: 'string'}
        ]
    }  
});  