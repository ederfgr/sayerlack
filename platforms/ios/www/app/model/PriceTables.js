Ext.define('App.model.PriceTables', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
        	{name: 'id', type: 'auto'},
            {name: 'code',  type: 'string'},  
            {name: 'codeItem',  type: 'string'},
            {name: 'un', type: 'string'},
            {name: 'price', type: 'string'}
        ]
    }
});