Ext.define('App.model.OrderType', {  
    extend: 'Ext.data.Model',  
    config: {  
        fields: [  
        	{name: 'id'},
            {name: 'code',  type: 'string'},  
            {name: 'desc',  type: 'string'}
        ],
        //idProperty: 'code' 
    }
});