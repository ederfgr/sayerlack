Ext.define('App.store.PriceTablesRemote', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.PriceTables',  
        autoLoad :false,
        autoDestroy: true,
        proxy: {  
            type: 'ajax',
            limitParam: false,
            timeout: 300000,
            useDefaultXhrHeader: false,
            pageParam: false,              
            reader: {  
                type: 'json',  
                rootProperty:'pricetables'
            }
        }
    }  
}); 