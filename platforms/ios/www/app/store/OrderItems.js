Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.OrderItems', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.OrderItems',  
        autoLoad: true,  
        sorters: 'itemcode',  
        /*grouper : function(record) {  
            return record.get('itemcode')[0];  
        },  */
  
        pageSize: 9999,  
        proxy: {
            type: "sql",
            limitParam: false,
            pageParam: false,    
            database: "ipadrev",
            table: 'orderitems'
        }
    }  
});  