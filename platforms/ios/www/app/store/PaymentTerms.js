Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.PaymentTerms', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.PaymentTerms',  
        autoLoad :true,  
        sorters: 'code',
        pageSize: 9999,                
        /*grouper : function(record) {  
            return record.get('code')[0];  
        },  */
        proxy: {
            type: "sql",
            limitParam: false,
            pageParam: false,    
            database: "ipadrev",
            table: 'paymentterms'
            //columns: ['id', 'code', 'desc']
        }
    }  
});