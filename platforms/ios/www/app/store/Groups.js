Ext.define('App.store.Groups', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Groups',  
        autoLoad :true,  
        sorters: 'code',  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/groups.json', 
            reader: {  
                type: 'json',  
                rootProperty:'groups'}  
        }  
    }  
});  