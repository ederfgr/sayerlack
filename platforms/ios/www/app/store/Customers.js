Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.Customers', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Customers',  
        autoLoad: true,
        sorters: 'name',
        pageSize: 9999,                
        grouper : function(record) {  
            return record.get('name')[0];  
        },  
        proxy: {
            type: "sql",
            limitParam: false,
            pageParam: false,    
            database: "ipadrev"
            //table: 'customers',
            //columns: ["code", "name", "cnpj", "abrev_name", "address", "city", "uf"]
        }
    }  
});