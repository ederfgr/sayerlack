Ext.define('App.store.Colors', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Colors',  
        autoLoad :true,  
        sorters: 'code',  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/colors.json', 
            reader: {  
                type: 'json',  
                rootProperty:'colors'}  
        }  
    }  
});  