Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.CustomerPaymentTerms', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.CustomerPaymentTerms',  
        autoLoad :true,  
        sorters: 'code',
        pageSize: 9999,                        
        proxy: {
            type: "sql",
            limitParam: false,
            pageParam: false,    
            database: "ipadrev",
            table: 'customerpaymentterms'            
        }
    }  
});