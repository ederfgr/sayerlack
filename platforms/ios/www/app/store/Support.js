Ext.define('App.store.Support', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Support',  
        autoLoad :true,  
        sorters: 'name',  
        grouper : function(record) {  
            return record.get('filename')[0];  
        },  
  
        proxy: {
            type: "sql",
            limitParam: false,
            timeout: 30000,
            pageParam: false,    
            database: "ipadrev",
            table: 'support'
        }
    }  
});  