Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.Settings', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Settings',  
        autoLoad :true,  
        sorters: 'urlws',        
          
        proxy: {
            type: "sql",
            timeout: 30000,
            database: "ipadrev",
            table: 'settings',
            columns: ["bloqIpad","lastorder","sitelist","urlws","username","password","id"]
        },
        listeners: {
            load: function(store, records, successful, operation, eOpts){

                if (successful == true){
                    if(records == ''){

                        var newRec = Ext.create('App.model.Settings', {
                            id: 1,
                            bloqIpad: "yes",
                            lastorder: "12345",
                            lastAcc: "",
                            minMsg: "",
                            minValue: 0,
                            sitelist: "",
                            urlws: "https://pedidos1.sayerlack.com.br:8091/scripts/cgiip.exe/WService=emsweb/service/",
                            username: "",
                            password: ""
                        });

                        //var contSql = App.app.getController('Sql');
           
                        contSql.insert (db, 'settings',
                            newRec.data,
                            function (results) {
                                store.sync();
                            },
                            function(err) {
                                Ext.Msg.alert('Erro Settings', err.message, Ext.emptyFn);
                            });
           
                        return newRec;
                    }
                }
            }
        }
    }  
});  
