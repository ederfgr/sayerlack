Ext.define('App.store.Products', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Products',  
        autoLoad :true,
        remoteFilter: false,
        sorters: 'code',  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/products.json', 
            reader: {  
                type: 'json',  
                rootProperty:'products'}  
        }  
    }  
});  