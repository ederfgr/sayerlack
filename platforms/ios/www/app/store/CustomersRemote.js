Ext.define('App.store.CustomersRemote', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Customers',  
        autoLoad :false,
        autoDestroy: true,
        sorters: 'name',        
        grouper : function(record) {  
            return record.get('name')[0];  
        },  
        proxy: {
            type: 'ajax',
            limitParam: false,
            timeout: 300000,
            pageParam: false,
            useDefaultXhrHeader: false,              
            reader: {  
                type: 'json',  
                rootProperty:'customers'
            }
        }
    }  
}); 