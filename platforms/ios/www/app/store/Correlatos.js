Ext.define('App.store.Correlatos', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Correlatos',  
        autoLoad :true,  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/correlatos.json', 
            reader: {  
                type: 'json',  
                rootProperty:'correlatos'}  
        }  
    }  
});  