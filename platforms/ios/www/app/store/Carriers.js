Ext.define('App.store.Carriers', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Carriers',  
        autoLoad :true,  
        sorters: 'abrevName',
        pageSize: 10000,
        grouper : function(record) {  
            return record.get('abrevName')[0];  
        },  
        proxy: {
            type: "sql",
            limitParam: false,
            timeout: 30000,
            pageParam: false,
            database: "ipadrev",
            table: 'carriers'
        }
    }  
});

