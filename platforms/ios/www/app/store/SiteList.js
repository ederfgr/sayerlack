Ext.define('App.store.SiteList', {
    extend : 'Ext.data.Store',
    
    config: { 
        model: 'App.model.SelectOptions',  
        autoload: false
    }

});