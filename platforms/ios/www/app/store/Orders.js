Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.Orders', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Orders',  
        autoLoad: true,  
        //autoSync: true,
        sorters: 'custnum',
        pageSize: 9999,  
        proxy: {
            type: "sql",
            limitParam: false,
            pageParam: false,    
            database: "ipadrev",
            table: 'orders'
        }
        /*,
        listeners: {
            load: {
                fn: function(s,r,o){
                    var customers = Ext.getStore('Customers');

                    customers.clearFilter(true);

                    r.forEach(function(record) {

                        var name = customers.findRecord('code', record.data.custnum);

                        record.data.custname = record.data.custnum + '-' + name.data.name;
                    }, this)
                }
            }
        }*/
    }  
});