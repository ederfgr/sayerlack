Ext.require(['Ext.data.proxy.SQL']);

Ext.define('App.store.PriceTables', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.PriceTables',  
        autoLoad :true,  
        //sorters: 'code',
        pageSize: 9999,                
        /*grouper : function(record) {  
            return record.get('code')[0];  
        },  */
        proxy: {
            type: "sql",
            limitParam: false,
            useDefaultXhrHeader: false,
            pageParam: false,    
            database: "ipadrev",
            table: 'pricetables'
        }
    }  
});