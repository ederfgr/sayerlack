Ext.define('App.store.ItemsGroups', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.ItemsGroups',  
        autoLoad :true,  
        sorters: 'groupname',  
        /*grouper : function(record) {  
            return record.get('groupname')[0];  
        },  */
  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/itemsgroups.json', 
            reader: {  
                type: 'json',  
                rootProperty:'groups'}  
        }  
    }  
});  