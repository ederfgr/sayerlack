Ext.define('App.store.Lines', {  
    extend: 'Ext.data.Store',  
    config: {  
        model: 'App.model.Lines',  
        autoLoad :true,  
        sorters: 'code',  
        proxy: {  
            type: 'ajax',  
            url : 'app/data/lines.json', 
            reader: {  
                type: 'json',  
                rootProperty:'lines'}  
        }  
    }  
});  