contCores = 0;

Ext.define('App.view.order.AddItemForm',{
	
	extend: 'Ext.form.Panel',
	alias: 'widget.additemform',
	
	config: {
		layout: 'fit',	
		scrollable: null,
        hidden: true,
		items: [
			{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            //title: 'Itens',
	            id: 'toolbarAddItemForm',
	            items: [
	                {
	                    xtype: 'button',
	                    ui: 'back',
	                    action: 'voltarItemsGroupsList',
	                    id: 'btBackItemsGroup',
	                    text: 'Voltar'
	                }
                ]
	        },
	        {
		        xtype: 'panel',	
		        ui: 'light',
		        scrollable: null,		        
		        tabBar: { layout: { pack: "center" } },
		        layout: 'fit',
		        height: '100%',
		        width: '100%',		        
	            items: [
	                {	                    
	                    //height: '300%',
	                    items: {
	                        xtype: 'dataview',
	        				itemId: 'colorsDataView',
	        				id: 'colorsDataView',
	        				height: '100%',
                            //width: null,
						    //baseCls: 'categories-list',
						    store: 'Colors',
						    cls:'list',
                            listeners: {
                                refresh: function(view, eOpts) {
                                    Ext.Viewport.setMasked(false);
                                    //console.log('refresh');
                                }
                            },

					        itemTpl: new Ext.XTemplate(
					        	'<div>',
					        	'<div class="color" style="background-image:url(resources/img/cores/{image})">',
					        	'<div class="itemDesc"> {desc} </div>',
					        	'</div>',
					        	'</div>',
				                    //'</p>',
				                    '<table border=0>',
				                    '<tr>',
					                    '<td align="center"> <h4> Qtde: </h4></td>',
					                    '<td align="center"> <h4> Un: </h4></td>',
					                    '<td align="center"> <h4> %Desc: </h4></td>',
					                    '<td align="center"> <h4> Valor: </h4></td>',
				                    '</tr>',
				                    '{[this.products(values.code)]}',
				                    '<br>',
				                    '</table>',
                                    '<br>',
			                    '</div>',
			                    {
			                    	compiled: true,
			                    	products: function(code) {
                                                       
			                    		var colorStore = Ext.getStore('Colors');

			                    		var productStore = Ext.getStore('Products');

			                    		var codePai = code;

			                    		var tpl = '';
                                                       
			                    		productStore.clearFilter(true);
                                                       
			                    		//var group = Ext.getCmp('groupbutton').getPressedButtons();

			                    		var colors = productStore.getData().filterBy(function(record) {
											return record.get('itemPai') === codePai;
										});
                                                                                               
			                    		var itemStore = Ext.getStore('OrderItems').getData();

			                    		var order = Ext.getStore('Orders').getData();
                                                       
                                                       //console.log('criaitem5');

			                    		colors.items.forEach(function(rec) {
                                                                                                                          
			                    			var item = itemStore.findBy(function(record) {
			                    				return record.get('itemcode') === rec.get('code');
			                    			});

			                    			var loteMulti = rec.get("loteMulti");
			                    			var codeItem  = rec.get('code');
                                                             
                                            if (codeItem.indexOf('/') != -1)
                                                codeItem = codeItem.split('/')[0];

			                    			if (order.items[0].data.ordertype == "30" || order.items[0].data.ordertype == "22")
			                    				loteMulti = 0;
                                                             
			                    			var priceTables = Ext.getStore('PriceTables').getData().findBy(function(tables){
			                    				return tables.get('codeItem').trim() == codeItem.trim();
			                    			});

			                    			var priceAux = 0;

			                    			if (priceTables != null)
			                    				priceAux = priceTables.get('price');

			                    			//Exibir o preço da tabela com o índice financeiro???

			                    			if (item != null) 
			                    				tpl = tpl + '<tr input id="__'+ codeItem +'"><td> <input type="number" onKeyDown="JavaScript: return jsDecimals(event)" onBlur="JavaScript:validLote(this,' + loteMulti + ')" name="qtdeItemPed" style="width:40px;border-radius:0px 0px;border-color: white" value="' + item.get('quantity') + '"/></td><td> <h4 name="unidItemPed"> &nbsp;'+ rec.get('unidade') +'&nbsp;</h4></td><td> <input type="number" onchange="javascript:trataDesconto(this,' +  Number(priceAux).toFixed(2) + ')" onKeyDown="JavaScript: return jsDecimals(event)" name="discItemPed" style="width:40px" value="'+ item.get('discount') +'"/></td><td><input type="number" onchange="javascript:trataDesconto(this,' +  Number(priceAux).toFixed(2) + ')" onKeyDown="JavaScript: return jsDecimals(event)" name="valorItemPed" style="width:100px" size=3 value="' + Number(item.get('value')).toFixed(2) + '"/></td></tr>';
			                    			else
			                    				tpl = tpl + '<tr input id="__'+ codeItem +'"><td> <input type="number" onKeyDown="JavaScript: return jsDecimals(event)" onBlur="javascript:validLote(this,' + loteMulti + ')" name="qtdeItemPed" style="width:40px;border-radius:0px 0px;border-color: white"/></td><td> <h4 name="unidItemPed"> &nbsp;'+ rec.get('unidade') +'&nbsp;</h4></td><td> <input type="number" onchange="javascript:trataDesconto(this,' +  Number(priceAux).toFixed(2) + ')" onKeyDown="JavaScript: return jsDecimals(event)" name="discItemPed" style="width:40px" value="0"/></td><td><input type="number" onchange="javascript:trataDesconto(this,' +  Number(priceAux).toFixed(2) + ')" onKeyDown="JavaScript: return jsDecimals(event)" name="valorItemPed" style="width:100px" size=3 value="' + Number(priceAux).toFixed(2) + '"/></td></tr>';
                                                             
			                    		});
                                                                                               
                                        return tpl;
			                    	}
			                    }
		                    )
	                    }
	                }
	            ]
	        }    		
        ]
	}
});

function trataDesconto(field,valorTabela) {
	var itemPedido = field.parentNode.parentNode;

	for (var i = 0; i < itemPedido.childElementCount; i++){
        
        var child = itemPedido.children[i].children[0];
                
        if (child.name != undefined){
            if (child.name == "qtdeItemPed") {
                qtdItem = child;
            } else {
                if (child.name == "discItemPed") {
                    discItem = child;
                } else {
                    valorItem = child;
                }
            }
        }
    }
	//console.log(field.parent.);
    if (field.name == 'discItemPed') {
        valorItem.value = valorTabela;
    }

    if (field.name == 'valorItemPed') {
        discItem.value = 0;
    }
}

function validLote(div, lote) {
	if (lote > 1) {
		if (div.value % lote > 0) {
			div.style.borderColor = 'red';
			div.style.borderRadius = '5px 5px';
			div.value = '';

			Ext.Msg.alert("Quantidade Inválida!", "Quantidade do item deve ser multiplo de " + lote, Ext.emptyFn);
		} else {
			div.style.borderColor = 'white';
			div.style.borderRadius = '0px 0px';
		}
	}
}	

function jsDecimals(e){
  var evt = (e) ? e : window.event;
  var key = (evt.keyCode) ? evt.keyCode : evt.which;

  if(key != null) 
  {
      key = parseInt(key, 10);
      
      //console.log('key - ' + key);
      if((key < 48 || key > 57) && (key < 96 || key > 105)) 
      {
          if(!jsIsUserFriendlyChar(key, "Decimals"))
          {
              return false;
          }
      }
      else
      {
          if(evt.shiftKey)
          {
              return false;
          }
      }
  }
  return true;
}  

function jsNumbers(e) 
{
  var evt = (e) ? e : window.event;
  var key = (evt.keyCode) ? evt.keyCode : evt.which;
  if(key != null) 
  {
      key = parseInt(key, 10);
      if((key < 48 || key > 57) && (key < 96 || key > 105)) 
      {
          if(!jsIsUserFriendlyChar(key, "Numbers"))
          {
              return false;
          }
      }
      else
      {
          if(evt.shiftKey)
          {
              return false;
          }
      }
  }
  return true;
} 

//------------------------------------------
// Function to check for user friendly keys
//------------------------------------------
function jsIsUserFriendlyChar(val, step) 
{
  // Backspace, Tab, Enter, Insert, and Delete
  if(val == 8 || val == 9 || val == 13 || val == 45 || val == 46)
  {
      return true;
  }
  // Ctrl, Alt, CapsLock, Home, End, and Arrows
  if((val > 16 && val < 21) || (val > 34 && val < 41))
  {
      return true;
  }
  if (step == "Decimals")
  {
      if(val == 188 || val == 110)
      {
          return true;
      }
  }
  // The rest
  return false;
}