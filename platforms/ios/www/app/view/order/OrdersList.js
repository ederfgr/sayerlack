Ext.define('App.view.order.OrdersList', {  
    extend: 'Ext.List',
    alias:'widget.orderslist',
    xtype: 'orderslist',  
    fullscreen: true,  
    id: 'orderslist',     
    config:{
        //onItemDisclosure: true,
        disableSelection:true,
        //pressedDelay: 1,
        store: 'Orders',  
        title: 'Pedidos',
        layout:{
            type: 'fit',
            animation: {
                type: 'fade'
            }
        },
        plugins: {
            xclass: 'Ext.plugin.SlideToRemove',
            buttonWidth: '10%',
            removeText: 'Eliminar'
        },
        itemTpl: new Ext.XTemplate(
            '<div width = "100%">',      
            '<div class="avatar" style="background-image: url(resources/img/{status}.png);width: 3%; max-width:3%; min-width:3%; background-repeat:no-repeat;"></div>',                       
            '<div align="center" style="display: inline-block;overflow: hidden;width: 40%;word-wrap:break-word;max-width:40%; vertical-align:top;"><h4>Cliente: </h4>{custname}</div>',
            '<div align="center" style="display: inline-block;overflow: hidden;width: 10%;word-wrap:break-word;max-width:10%; vertical-align:top;"><h4>Pedido EMS: </h4>{ordernum}</div>',
            '<div align="center" style="display: inline-block;overflow: hidden;width: 10%;word-wrap:break-word;max-width:10%; vertical-align:top;"><h4>Pedido Cliente: </h4>{ordercust}</div>',
            '<div align="center" style="display: inline-block;overflow: hidden;width: 14%;word-wrap:break-word;max-width:14%; vertical-align:top;"><h4>Data Emissão: </h4>{createdate}</div>',
            '<div align="center" style="display: inline-block;overflow: hidden;width: 13%;word-wrap:break-word;max-width:13%; vertical-align:top;"><h4>Data Entrega: </h4>{deliverydate}</div>',
            '<div align="center" style="display: inline-block;overflow: hidden;width: 10%;word-wrap:break-word;max-width:10%; vertical-align:top;"><h4>Valor Mercadoria: </h4>{[this.valueFormat(values.value)]}</div>',
            {
                compiled: true,
                valueFormat: function(value) {

                    if (Number(value)) {
                        value = String(Number(value).toFixed(2)).replace('.', ',');
                    }

                    return value;
                }
            }
        ),
        items:[{  
            xtype:'toolbar',                                       
            docked:'top',  
            items:[{  
                xtype: 'searchfield',                          
                itemId:'orders_search',  
                id:'orders_search',                         
                placeHolder: ' Buscar Pedidos',
                listeners: {
                   keyup: function(field, e, eOpts) {
                        var button = Ext.getCmp('statusbutton');
                   
                        button.setPressedButtons(4);
                   }
                }
            }, {
                xtype: 'segmentedbutton',
                id: 'statusbutton',
                padding: '8 5 8 5', 
                docked: 'right',
                //pressedCls: 'customPressedCls',
                width: '55%',
                allowMultiple: false,
                items: [{
                    text: 'Proposta',
                    pressed: false,
                    width: '25%',
                    iconCls: 'pendente',
                    iconMask: true
                }, {
                    text: 'A Integrar',
                    pressed: false,
                    width: '20%',
                    iconCls: 'liberado',
                    iconMask: true
                }, {
                    text: 'Efetivado',
                    pressed: false,
                    width: '20%',
                    iconCls: 'integrado',
                    iconMask: true
                }, {
                    text: 'Erro',
                    pressed: false,
                    width: '15%',
                    iconCls: 'erro',
                    iconMask: true
                }, {
                    text: 'Todos',
                    pressed: true,
                    width: '15%'
                }],
                listeners: {
                   toggle: function(container, button, isPressed, eOpts) {
                   
                        var orderStore = Ext.getStore('Orders');
                        var pressed = container.getPressedButtons();
                   
                        if (isPressed == true) {
                            var search = Ext.getCmp('orders_search');
                   
                            if (search.getValue().length > 1)
                                search.reset();
                   
                            orderStore.clearFilter();
                   
                            if (button.getText() != 'Todos') {
                   
                                orderStore.filterBy(function(order) {
                                    var ret = false;
                                                    
                                    for (var i = pressed.length -1; i >= 0; i--) {
                                        if (ret == false) {
                                            if(pressed[i].getText() == "Erro") {
                                                if (order.get('status') == "Errosync")
                                                    ret = true;
                                            } else {
                                                if (pressed[i].getText() == "A Integrar"){
                                                    if (order.get('status') == "Liberado")
                                                        ret = true;
                                                } else {
                                                    if (pressed[i].getText() == "Efetivado"){
                                                        if (order.get('status') == "Integrado")
                                                            ret = true;                                                    
                                                    } else {
                                                        if (order.get('status') == "Pendente")
                                                            ret = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    return ret;
                                });

                                orderStore.sort('custnum', 'ASC');

                            } else {
                                orderStore.sort(
                                    {
                                        sorterFn: function(order1, order2) {

                                            var date1 = new Date(order1.get('createdate').split("/")[2],
                                                                 order1.get('createdate').split("/")[1] - 1,
                                                                 order1.get('createdate').split("/")[0]),
                                                date2 = new Date(order2.get('createdate').split("/")[2],
                                                                 order2.get('createdate').split("/")[1] - 1,
                                                                 order2.get('createdate').split("/")[0]);

                                            return (date1 > date2) ? 1 : (date1 === date2 ? 0 : -1);
                                        },
                                        direction: 'DESC'
                                    }
                                );
                            }
                        }
                   },
                   painted: function(button, eOpts) {
                        var pressed = this.getPressedButtons();

                        var orderStore = Ext.getStore('Orders');

                        if (pressed[0].getText() != 'Todos') {
                            orderStore.sort('custnum', 'ASC');
                        } else {

                            orderStore.sort(
                                {
                                    sorterFn: function(order1, order2) {

                                        var date1 = new Date(order1.get('createdate').split("/")[2],
                                                             order1.get('createdate').split("/")[1] - 1,
                                                             order1.get('createdate').split("/")[0]),
                                            date2 = new Date(order2.get('createdate').split("/")[2],
                                                             order2.get('createdate').split("/")[1] - 1,
                                                             order2.get('createdate').split("/")[0]);

                                        return (date1 > date2) ? 1 : (date1 === date2 ? 0 : -1);
                                    },
                                    direction: 'DESC'
                                }
                            );

                        }
                        //this.fireEvent('toggle', this, pressed[0], true);
                   
                   }
                }
            }]
        }]
    }  
});  
