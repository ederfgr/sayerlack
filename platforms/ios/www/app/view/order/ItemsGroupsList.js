var codeGroup = null;
Ext.define('App.view.order.ItemsGroupsList',{
	
	extend: 'Ext.form.Panel',
	alias: 'widget.itemsgroupslist',
	
	config: {
		layout: { 
			type: 'fit',
			animation: {
				type: 'pop'
			}
		},
		scrollable: null,
		items: [
			{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            title: 'Itens',
	            itemId: 'toolbarItemsGroupsList',
	            items: [
	                {
	                    xtype: 'button',
	                    ui: 'back',
	                    action: 'voltarOrderForm',
	                    text: 'Pedido'
	                }
	            ]
	        },	        
    		{
    			xtype: 'panel',	        	
	        	//width: '85%',	        	
	        	docked: 'left', 
	        	layout: 'fit',
	        	flex: 1,

	        	width: '25%',
	        	items: [
	        	{
					xtype: 'list',
					//ui: 'round',
					store: 'ItemsGroups',
					itemId: 'listItemsGroups',
					width: '200',
					height: '400',
					//flex: 25,
					style: 'background-color:#2E2E2E',
					//disableSelection:true,
					itemTpl: new Ext.XTemplate(
						'<div>{[this.name(values.groupname)]}</div>',
						{
							compiled: true,
							name: function(name) {
								return name.replace("ZZZ","");
							}
						}
						/*'{[this.color(values.quantidade,values.groupname,values.code)]}',
						{
							compiled: true,
							color: function(qtde,groupname,codegroup) {
                                               
								if (qtde == 0)
									qtde = 1;
                                               
								var tpl,
									items = Ext.getStore('OrderItems').getData(),
									productStore = Ext.getStore('Products'),
									colorStore = Ext.getStore('Colors'),
									lineStore = Ext.getStore('Lines'),
									contItem = 0,
									lineItem = "NO";
                                               
								lineStore.clearFilter(true);
								colorStore.clearFilter(true);

								lineStore.filterBy(function(line) {
									return line.get('codeGroup') === codegroup;
								});

								lineStore.data.each(function(line){
                                                    

									lineItem = "NO";

									items.findBy(function(item) {
										var itemPai = productStore.getData().findBy(function(prod) {
											return prod.get('code') == item.get('itemcode');
										});

										//var itemPai = productStore.getAt(products).getData();
                                                 

										var color = colorStore.findBy(function(recColor) {
											return recColor.get('code') === itemPai.get('itemPai');
										});

										var lineCode;
                                                 
										if (color != -1)
											lineCode = colorStore.getAt(color).getData().lineCode;
                                                 
										if (lineCode === line.get('code'))
											lineItem = "YES";
                                                 
									});

									if (lineItem == "YES")
										contItem++;
								});
                                               
								lineStore.clearFilter(true);
                                               
                                var percent;
                                                                                              
                                if (groupname == "ZZZPromocao")
                                    percent = '';
                                else
                                    percent = Number((contItem * 100) / qtde).toFixed(2) + '%';
                                               
								tpl = '<div input id=group_' + codegroup + ' style="background-color: rgba(140, 154, 255, 0.5); border-radius: 10px 10px; width: ' + ((contItem * 100) / qtde) + '%; max-width:100%; no-repeat auto">' + groupname.replace("ZZZ","") + '<div style="float:right; position:absolute; right:20px; top: 16px;z-index:-1;color:rgb(200,200,200);font-size:65%">' + percent + '</div></div>';

								return tpl;
							}
						}*/
					),
					allowDeselect: true,
					listeners: {
						select: function (list, record, eOpts ) {
                            if (record.data.groupname != "ZZZOportunidade") {
                                codeGroup = record.data.code;

                                var storeLines = Ext.getStore('Lines');

                                storeLines.clearFilter();

                                storeLines.filter('codeGroup', codeGroup);
								
                            }
                            else {
                            	var path = docDir.fullPath + '/Promocao.jpg';
    							//path = path.substr( 0, path.length - 10 );
                                //window.plugins.childBrowser.showWebPage("file://" + path + "resources/img/Promocao.jpg",{ showLocationBar: true , showAddressBar: false});
                        
                                window.open("file://" + path, '_blank', 'hidden=no,enableViewportScale=yes,allowInlineMediaPlayback=yes,location=no');
                        
                                //window.plugins.childBrowser.showWebPage("file://" + path,{ showLocationBar: true , showAddressBar: false});
                        
                                list.deselectAll();


                            }
					    },
					    deselect: function (list, record, supressed, eOpts ) {
                            if (record.data.groupname != "ZZZOportunidade") {
                                var storeLines = Ext.getStore('Lines');

                                storeLines.clearFilter();
                            }
                            codeGroup = null;
					    }
					}
            
				}]
			},
	        {
	        	xtype: 'panel',	        	
	        	//width: '85%',	        	
	        	align: 'stretch',
	        	itemId: 'subGroupsContainer',	        	

	        	flex: 2,
	        	items: [
	        		{
		        		xtype:'toolbar',                                       //  bottom toolbar  
			            docked:'top',  
			            items:[{  
			                xtype: 'searchfield',                          //  here is the searchfield  
			                itemId:'items_search',  
			                id:'items_search',                         //   we will be using this id in the controller  
			                placeHolder: ' Buscar Produto'  
			            }]
			        }, 
	        		{	
	        			xtype: 'dataview',
        				itemId: 'subGroupsDataView',
        				id: 'subGroupsDataView',	        			
        				height: '100%',
                        pressedDelay: 1,
					    //baseCls: 'dataviewLines',				    

					    store: 'Lines',
					    cls:'list',
					    style:'vertical-align:top',
				        itemTpl: new Ext.XTemplate(
			               '<div class="item" style="vertical-align:top">',
			                    '<div class="imageLines" style="background:url(resources/img/linhas/{image}) no-repeat 0"></div>',
			                    '<p class="title">{desc}</p>',
			                    '<h4>',
			                    '<p>{attr1}</p>',
			                    '<p>{attr2}</p>',
			                    '<p>{attr3}</p>',
			                    '<p>{attr4}</p>',
			                    '</h4>',
			                    '<br>',
			               '</div>'
         				)						
					}
	        	]
	        }
                ]
	}
});
