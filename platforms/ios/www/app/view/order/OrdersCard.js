Ext.define('App.view.order.OrdersCard', {

	extend: 'Ext.NavigationView',
	xtype: 'ordersContainer',

	config: {
        optimizedItems: null,
		navigationBar: false,

        tab: {
			title: 'Pedidos',
	        iconCls: 'bookmarks',
	        iconMask: true,
            icon: 'true',
            hidden: null,
            //style: 'visibility: inherit',
            style: 'visibility: visible;',
	        action: 'OrdersTab'
	    },
	    layout: {
            animation: {
                type: 'fade'
            }
        },

        autoDestroy: true,

		items: [
			/*{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            title: 'Pedidos'
	        },*/
			{
				xtype: 'orderslist'
			}
                ],
           listeners: {
           activate: function (me, newActiveItem, oldActiveItem, eOpts) {
           var items = me.getOptimizedItems();
                      
           Ext.getStore('Orders').clearFilter();
           
           if (!items) {
           return;
           }
                      
           me.add(items);
           },
           deactivate: function(tab) {
           tab.setOptimizedItems(tab.getActiveItem());
           tab.removeAll(false, true);
           
           }
           }
	}
});
