//Ext.require(['Ext.plugin.SlideToRemove']);

Ext.define('App.view.order.OrdersForm',{
	
	extend: 'Ext.form.Panel',
	alias: 'widget.ordersform',
	store: 'Orders',
	//model: 'Orders',
	config: {
		layout: {
			type: 'fit',
			animation: {
				type: 'fade'
			} 
		},
		margin: '0px',
		//fullscreen: true,
		items: [
			{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            title: 'Pedido',
	            itemId: 'toolbarOrdersForm',
	            items: [
	                {
	                    xtype: 'button',
	                    ui: 'back',
	                    action: 'voltarOrdersList',
	                    text: 'Pedidos'
	                },
	                {
	                    xtype: 'button',
	                    ui: 'confirm',
	                    action: 'saveOrder',
	                    id: 'btSaveOrder',
	                    iconCls: 'download',
	                    iconMask: true,
	                    //text: 'Salvar',
	                    docked: 'right'
	                    //icon: 'resources/img/novo/save.png'
	                },
	                {
	                    xtype: 'button',
	                    ui: 'decline',
	                    action: 'deleteOrder',
	                    id: 'btDeleteOrder',
	                    iconCls: 'trash',
	                    iconMask: true,
	                    //text: 'Excluir',
	                    //icon: 'resources/img/novo/delete.ico',
	                    docked: 'right'
	                },
	                {
	                	xtype: 'button',
	                	iconCls: 'action',
    					iconMask: true,
    					docked: 'right',
	                	ui: 'action',
	                	action: 'syncOrder',
	                	id: 'btSyncOrder'
	                	
	                	//icon: 'resources/img/novo/sync.png'
                    },
                    {
	                    xtype: 'button',
	                    ui: 'action',
	                    action: 'sendMail',
	                    id: 'btSendMail',
	                    iconCls: 'mail',
	                    iconMask: true,
	                    //text: 'Salvar',
	                    docked: 'right'
	                    //icon: 'resources/img/novo/save.png'
                    },
                    {
                        xtype: 'button',
                        ui: 'action',
                        action: 'copyOrder',
                        id: 'btCopyOrder',
                        iconMask: true,
                        //text: 'Salvar',
                        docked: 'right',
                        iconCls: 'copy'
                    }
	            ]
	        },
	        {	        	
	        	xtype: 'panel',
	        	id: 'panelOrderPrinc',
	        	flex: 1,
	        	layout: 'hbox',
	        	docked: 'top',
                scrollable: null,
	        	margin: '0px 15px',
	        	items: [
	        		{
			        	xtype: 'fieldset',
			        	title: '',		        	
			        	width: '25%',			        	
			        	layout: {pack: 'start'},
			        	style: "margin: 0px",
			        	items: [
			        		
			        		{
			        			xtype: 'textfield',
			        			name: 'ordernum',
			        			id: 'ordernum',
			        			label: 'Pedido EMS',
			        			labelWidth: '50%',
			        			originalValue: '',
			        			disabled: true
			        		},
			        		{
			        			xtype: 'textfield',
			        			name: 'ordercust',
			        			id: 'ordercust',
			        			label: 'Ordem Compra',
			        			labelWidth: '50%',
			        			originalValue: '',
			        			disabled: false
			        		},
			        		{
			        			xtype: 'selectfield',
			        			name: 'siteid',
			        			id: 'fdSiteId',
			        			labelWidth: '50%',
			        			label: 'Estabelecimento',
			        			originalValue: '',
			        			disabled: false,
			        			store:  'SiteList',
			        			placeHolder: 'Estabelecimento...'
			        		}
			        	]
			        },
			        {
			        	xtype: 'fieldset',
			        	title: '',		        	
			        	width: '25%',
			        	layout: {pack: 'center'},
			        	style: "margin: 0px",
			        	items: [
			        		{
			        			xtype: 'selectfield',
			        			name: 'ordertype',
			        			labelWidth: '50%',			        			
			        			label: 'Tipo Pedido',
			        			id: 'fdOrderType',
			        			originalValue: '',
			        			disabled: false,
			        			store: 'OrderType',
			        			displayField: 'desc',
			        			valueField: 'code'
			        		},
			        		{
			        			xtype: 'selectfield',
			        			name: 'paymentTerms',
			        			id: 'paymentTerms',
			        			labelWidth: '50%',
			        			label: 'Cond Pagto',
			        			originalValue: '',
			        			disabled: false,
			        			store: 'PaymentTerms',
			        			displayField: 'desc',
			        			valueField: 'code',
			        			listeners: 
								{								    
								    change: function(selectbox,newValue,oldValue)
								    {
								    	var paymentTermsStore = Ext.getStore('PaymentTerms');
								    	var CustomerPaymentTermsStore = Ext.getStore('CustomerPaymentTerms');

								        var condPagto = paymentTermsStore.getData().findBy(function(rec) {
											return rec.data.code == newValue;
										});

										if (condPagto != null) {
											Ext.getCmp('financialIndex').setValue(condPagto.data.financialIndex);
										};

										var custTerm = CustomerPaymentTermsStore.getData().findBy(function(rec) {											
											return rec.data.paymentTerm == newValue && rec.data.custnum == custnum ;
										});

										if (custTerm != null) {
											Ext.getCmp('financialIndex').setValue(custTerm.data.financialIndex);
										};
								    }
								}  
			        		},
			        		{
			        			xtype: 'textfield',
			        			name: 'financialIndex',
			        			id: 'financialIndex',
			        			label: 'Tx Financ',
			        			//originalValue: '',
			        			labelWidth: '50%',
			        			disabled: true
			        		}			        		
			        	]
			        },
			        {
			        	xtype: 'fieldset',
			        	title: '',		        	
			        	width: '25%',
			        	layout: {pack: 'end'},
			        	style: "margin: 0px",
			        	items: [
			        		{
			        			xtype: 'datepickerfield',
			        			name: 'createdate',
			        			dateFormat: 'd/m/Y',
			        			labelWidth: '50%',
			        			label: 'Emissão',
			        			originalValue: '',
			        			disabled: true,
			        			id: 'createdate',
			        			value: {
			        				day: (new Date().getDate()),
			        				month: (new Date().getMonth() + 1),
			        				year: (new Date().getFullYear())
			        			}
			        		},			        		
			        		{
			        			xtype: 'datepickerfield',
			        			name: 'deliverydate',
                                destroyPickerOnHide: true,
			        			label: 'Entrega',
			        			labelWidth: '50%',
			        			originalValue: '',
			        			id: 'deliverydate',
			        			disabled: false,
			        			dateFormat: 'd/m/Y',
			        			value: {
			        				day: (new Date().getDate()),
			        				month: (new Date().getMonth() + 1),
			        				year: (new Date().getFullYear())
			        			},
			        			picker: {
			        				value: {day: new Date().getDate(), month: new Date().getMonth() + 1, year: new Date().getFullYear()},
			        				yearFrom: (new Date().getFullYear()),
			        				yearTo: 2099,
			        				slotOrder: ['day', 'month', 'year'] 
			        			}			        			
			        		},
			        		{
			        			xtype: 'numberfield',
			        			name: 'discount',
			        			label: 'Desc Inform',
			        			id: 'discount',
			        			//originalValue: '',
			        			labelWidth: '50%',
			        			disabled: false,
			        			component : {
			        				type: 'tel',
					                pattern : '[0-9]*'
					            }
			        		}
			        	]
			        },
			        {	        	
			        	xtype: 'fieldset',			        	
			        	layout: {pack: 'end'},	
			        	width: '25%',
			        	title: '',
			        	style: "margin: 0px",	
			        	items: [	
			        		{
								xtype: 'textfield',
			        			name: 'carrier',
			        			labelWidth: '50%',			        			
			        			label: 'Transportadora',
			        			id: 'carrier',
			        			originalValue: '',
			        			disabled: true,
                                readOnly: true,
			        			listeners: {
			        				focus: function(field, eOpts) {                                
                                
			        					var carriersPopup = Ext.create('Ext.Panel', {
							                fullscreen : true,
							                centered : true,
							                modal: true,
							                width : 320,
							                height : 380,            
							                layout : 'fit',            
							                id: 'carriersPopup',
							                items : [
							                    {
													xtype: 'carrierslist'
												}							                    
							                ],
							                scrollable : true,
                                            hideOnMaskTap: true,
                                            listeners: {
                                                hide: function(){
                                                    this.destroy();
                                                }
                                            }
							            });
							            carriersPopup.showBy(Ext.getCmp('carrier'));
			        				}
			        			}

							},
			        		{
			        			xtype: 'textfield',
			        			name: 'value',
			        			label: 'Vl Mercadoria',	
			        			id: 'valueMerc',
			        			originalValue: '',
			        			labelWidth: '50%',
			        			//ui: 'text',
			        			disabled: true,
			        			listeners: {
			        				painted: function(field, eOpts) {
			        					var value = Ext.getCmp(field.id);

			        					if (Number(value.getValue())) {
			        						value.setValue(Number(value.getValue()).toFixed(2));
			        					}			        					
			        				},
                                    change: function(field, eOpts) {
                                        var value = Ext.getCmp(field.id);
                                        
                                        if (Number(value.getValue())) {
                                            value.setValue(Number(value.getValue()).toFixed(2));
                                        }			        					
                                    }
			        			}		        			
			        		},
			        		{
			        			xtype: 'textfield',
			        			name: 'valueEMS',
			        			label: 'Vl Total',	
			        			id: 'valueTot',
			        			labelWidth: '50%',		        			
			        			originalValue: '',
			        			disabled: true,
			        			listeners: {
			        				painted: function(field, eOpts) {
			        					var value = Ext.getCmp(field.id);

			        					if (Number(value.getValue())) {
			        						value.setValue(Number(value.getValue()).toFixed(2));
			        					}			        					
			        				},
                                    change: function(field, eOpts) {
                                        var value = Ext.getCmp(field.id);
                                        
                                        if (Number(value.getValue())) {
                                            value.setValue(Number(value.getValue()).toFixed(2));
                                        }			        					
                                    }
			        			}
			        		}			        			        	
					    ]
					}
		        ]
	        },
	        {	        	
	        	xtype: 'panel',
	        	flex: 2,	        	
	        	docked: 'top',
	        	items: [
	        		{
			        	xtype: 'fieldset', 
			        	title: 'Observações',

			        	style: "margin: 0px 15px 0px 15px",	
			        	items: [
			        		{
			        			xtype: 'textareafield',
			        			name: 'remarks',
			        			id: 'remarks',
			        			height: '60px',
			        			originalValue: '',
			        			label: '',
			        			disabled: false,
			        			maxrows: 2
			        		}
			        	]
			        }
			    ]
			}, {
                xtype  : 'titlebar',
                title  : 'Itens do Pedido',
                id: 'titleBarItem',
                docked : 'top',
                padding: 10,
                items  : [
                    {
                        xtype  : 'button',
                        align  : 'left',
                        action: 'addItem',
                        id: 'btAddItem',
	                    //text: 'Incluir Item'
	                    iconCls: 'add',
    					iconMask: true,
    					ui: 'action'
                    }
                ]
            }, {
                xtype   : 'list',
                store: 'OrderItems',
                scrollable: null,
				disableSelection: true,
				id: 'listOrderItems',
				/*plugins: {
			        xclass: 'Ext.plugin.SlideToRemove',
			        buttonWidth: '20%',
			        removeText: 'Deletar'
			    },*/
				itemTpl: new Ext.XTemplate(
					'<div class="avatar" style="background-image: url({[this.getFatherImg(values.itemcode)]});width: 3%; max-width:3%; no-repeat auto"></div>',
				    '<div width = "100%">',				    
				    '<div align="center" style="display: inline-block;overflow: hidden;width: 16%;word-wrap:break-word;max-width:16%; vertical-align:top;"><h4>Item: </h4>{[values.itemcode.split("/")[0]]}</div>',
			        '<div align="center" style="display: inline-block;overflow: hidden;width: 33%;word-wrap:break-word;max-width:33%; vertical-align:top;"><h4>Descrição: </h4>{description}</div>',
			        '<div align="center" style="display: inline-block;overflow: hidden;width: 10%;word-wrap:break-word;max-width:10%; vertical-align:top;"><h4>Qtde: </h4>{[this.valueFormat(values.quantity)]}</div>',
			        '<div align="center" style="display: inline-block;overflow: hidden;width: 8%;word-wrap:break-word;max-width:8%; vertical-align:top;"><h4>Un: </h4>{un}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 12%;word-wrap:break-word;max-width:12%; vertical-align:top;"><h4>{[this.findTablePrice(values,0)]}: </h4>R$ {[this.findTablePrice(values,1)]}</div>',
                    '<div align="center" style="display: inline-block;overflow: hidden;width: 6%;word-wrap:break-word;max-width:6%; vertical-align:top;"><h4>% Desconto:</h4>{[this.findTablePrice(values,2)]}</div>',
                    '<div align="center" style="display: inline-block;overflow: hidden;width: 11%;word-wrap:break-word;max-width:11%; vertical-align:top;"><h4>Vl Líquido: </h4>R$ {[this.netPrice(values)]}</div>',
		            {
		            	compiled: true,
		            	valueFormat: function(value) {
		            		if (Number(value)) {
        						var value = String(Number(value).toFixed(2)).replace('.', ',');
        					}

        					return value;
		            	}
		            },
		            {
		            	compiled: true,
		            	netPrice: function(values) {
                            
		            		
		            		//Se o pedido estiver efetivado
		            		if (Ext.getCmp('btSaveOrder').isDisabled()) {
		            			if (Number(values.discount) == 0)
		            				return String(Number(values.value).toFixed(2)).replace('.', ',');
		            			else {
	            					var vlLiq = values.value - (values.value * values.discount / 100);
                                           
                                    if (Number(vlLiq)) {
                                        vlLiq = String(Number(vlLiq).toFixed(2)).replace('.', ',');
                                    }
                                           
	            					return vlLiq;
		            			}

		            		}
                                           
                            var vlLiq = values.value - (values.value * values.discount / 100);
                                           
                            fldTaxa = Ext.getCmp('financialIndex').getValue();
                                           
                            if (Number(fldTaxa) > 0)
                                vlLiq = vlLiq + (vlLiq * Number(fldTaxa) / 100);
                            else {
                                if (Number(fldTaxa) < 0)
                                    vlLiq = vlLiq - ((vlLiq * -1) * Number(fldTaxa) / 100);
                            }
                                           
                            if (Number(vlLiq)) { 
        						vlLiq = String(Number(vlLiq).toFixed(2)).replace('.', ',');
        					}
                                           
        					return vlLiq;
		            	}
		            },
		            {
		            	compiled: true,
			            findTablePrice: function(values,opcao) {
	                                   
	                        var priceTables = Ext.getStore('PriceTables').getData().findBy(function(tables){
	                            return tables.get('codeItem').trim() == values.itemcode.trim();
	                        });
	                                   
	                        var priceAux = 0;
	                                   
	                        if (priceTables != null)
	                            priceAux = Number(priceTables.get('price')).toFixed(2); 
	                        
	                        var desconto = (1 - (Number(values.value).toFixed(2) / priceAux)) * 100 ;
	                                
	                        var arr = [];
                                           
                            fldTaxa = Ext.getCmp('financialIndex').getValue();
                                           
                            if (Number(fldTaxa) > 0)
                                priceAux = Number(priceAux) + (Number(priceAux) * Number(fldTaxa) / 100);
                            else {
                                if (Number(fldTaxa) < 0)
                                    priceAux = Number(priceAux) - ((Number(priceAux) * -1) * Number(fldTaxa) / 100);
                            }
                                      
                            //console.log(values.value);
                            //console.log(Number(priceAux));
                                           
                            arr.push("Preço Lista");
                                           
                            if (Ext.getCmp('btSaveOrder').isDisabled()) {
                                arr.push(String(Number(values.value).toFixed(2)).replace('.', ','));
                                arr.push(String(Number(values.discount).toFixed(2)).replace('.', ','));
                            } else {
                                arr.push(String(Number(priceAux).toFixed(2)).replace('.', ','));
                                
                                if (Number(values.discount) == 0)
                                    arr.push(String(Number(desconto).toFixed(2)).replace('.', ','));
                                else
                                    arr.push(String(Number(values.discount).toFixed(2)).replace('.', ','));
                            }

                        	/*if(Number(desconto) && desconto > 0) {
                        		arr.push("Preço Lista");
                        		arr.push(String(Number(priceAux).toFixed(2)).replace('.', ','));
                        		if (Ext.getCmp('btSaveOrder').isDisabled()) { 
                        			if (Number(values.discount) == 0)
                        				arr.push(String(Number(desconto).toFixed(2)).replace('.', ','));
                        			else
                        				arr.push(String(Number(values.discount).toFixed(2)).replace('.', ','));

                        		}
                        		else
                        			arr.push(String(Number(desconto).toFixed(2)).replace('.', ','));
                        	}
                        	else {
                        		arr.push("Preço Lista");
                        		arr.push(String(Number(values.value).toFixed(2)).replace('.', ','));
                        		arr.push(String(Number(values.discount).toFixed(2)).replace('.', ','));
                        	}*/

	                        
	                        return arr[opcao];
	                    }
	                },
		            {
		            	compiled: true,
		            	getFatherImg: function(itemcode) {
		            		productsStore = Ext.getStore('Products');
		            		colorsStore = Ext.getStore('Colors');
		            		lineStore = Ext.getStore('Lines');
                                           
                            productsStore.data.setFilters(null);
                            colorsStore.data.setFilters(null);
                            lineStore.data.setFilters(null);

		            		itemcode = itemcode;

		            		var products = productsStore.getData().findBy(function(rec) {
								return rec.data.code == itemcode;
							});

							if (products == null) {
								return 'resources/img/linhas/itemsemimagem.jpg';
							};

		            		var color = colorsStore.getData().findBy(function(rec) {
								return rec.get('code') == products.data.itemPai;
							});

							if (color == null) {
								return 'resources/img/linhas/itemsemimagem.jpg';
							};

							var line = lineStore.getData().findBy(function(rec) {
								return rec.get('code') == color.data.lineCode;
							});							

        					return 'resources/img/linhas/' + line.data.image;
		            	}
		            }
			    )
			}			
		],
        listeners: {
           activate: function(me, newActiveItem, oldActiveItem, eOpts) {
           
           
           
	           var itemsStore = Ext.getStore('OrderItems'),
	               order = me.getRecord(),
	               orderType = Ext.getCmp('fdOrderType'),
	               orderIpad;
	           
	            if (order)
	         	   orderIpad = order.get('orderipad');
	            else
	           		orderIpad = 0;
	           
	            itemsStore.data.setFilters(null);
	            itemsStore.filterBy(function(rec) {
                    return rec.data.orderipad == orderIpad;
                });
	                      
	            if (itemsStore.getCount() != 0)
	           		orderType.setDisabled(true);
	            else
	           		orderType.setDisabled(false);
					
				//Não recalcular preço para pedidos já integrados
	           	if (Ext.getCmp('btSaveOrder').isDisabled()) 
	           		return;

	           	var value = 0;
	           	itemsStore.each(function(recItem) {
                	auxValue = Number(recItem.get('value')) * Number(recItem.get('quantity'));
  					value = (value) + (auxValue - (auxValue * recItem.get('discount') / 100));
  				});
           
                if(order && order.get('discount') > 0)
                    value = value - (value * order.get('discount') / 100);
           
                if(order && Number(me.getValues().financialIndex) > 0)
                    value = value + (value * Number(me.getValues().financialIndex) / 100);
                else {
                    if(order && Number(me.getValues().financialIndex) < 0)
                        value = value - (value * (-1 * Number(me.getValues().financialIndex)) / 100);
                }
           
                Ext.getCmp('valueMerc').setValue(value);

  				if (order) {
  					order.set("value",value);

	  				//var contSql = App.app.getController('Sql');
	  				contSql.update (db, 'orders',
                                    order.data,
                                    'orderipad="' + order.data.orderipad +'"',
                                    'AND',
                                    function (results) {                                        
                                    },
                                    function(err) {
                                        //console.log("Erro Pedido!");
                                    });
	  			}
           
           	}
        }
	}
});
