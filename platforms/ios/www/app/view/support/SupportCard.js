Ext.define('App.view.support.SupportCard', {

	extend: 'Ext.NavigationView',
	xtype: 'supportContainer',

	config: {
           
        optimizedItems: null,

        tab: {
			title: 'Material',
	        iconCls: 'info',	        
    		iconMask: true,
	        action: 'supportTab'
	    },

        autoDestroy: true,

		items: [
			{
				xtype: 'supportlist'
			}
		],
           listeners: {
           activate: function (me, newActiveItem, oldActiveItem, eOpts) {
           var items = me.getOptimizedItems();
           
           if (!items) {
           return;
           }
           
           me.add(items);
           },
           deactivate: function(tab) {
           tab.setOptimizedItems(tab.getActiveItem());
           tab.removeAll(false, true);
           }
           }
	}
});
