Ext.define('App.view.support.SupportList', {  
    extend: 'Ext.List',  
    alias:'widget.supportlist',
    xtype: 'supportlist',  
    fullscreen: true,  
    id: 'supportlist',     
    config:{  
        disableSelection:true,  
        store:'Support',  
        title: 'Material de Apoio' ,

        itemTpl: new Ext.XTemplate (
            '<div class="avatar" style="background-image: url(resources/img/{filetype}.jpg);"></div>',
            '<div class="update" style="background-image: url(resources/img/update{uptodate}.jpg);"></div>',
            '<h3>  {description}</h3>',
            '<h4>  Nova versão disponível: {uptodate}</h4>'
        ),
        grouped: true,
                
        itemCls: 'customer',

        //itemTpl:'{code} {name}',  
        items:[{  
            xtype:'toolbar',                                       //  bottom toolbar  
            docked:'top',  
            items:[{  
                xtype: 'searchfield',                          //  here is the searchfield  
                itemId:'support_search',  
                id:'support_search',                         //   we will be using this id in the controller  
                placeHolder: ' Buscar Material'  
            }]  
        }]  
    }  
});  