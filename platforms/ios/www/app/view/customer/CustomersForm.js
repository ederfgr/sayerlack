Ext.require(['Ext.plugin.SlideToRemove']);

Ext.define('App.view.customer.CustomersForm',{
	
	extend: 'Ext.form.Panel',
	alias: 'widget.customersform',
	
	config: {
		layout: {
			type: 'fit',
			animation: {
				type: 'fade'
			}
		},
		scrollable: null,
		ui: 'light',  	
		items: [
			{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            title: 'Cliente',
	            itemId: 'toolbarCustomersForm',
	            items: [
	                {
	                    xtype: 'button',
	                    ui: 'back',
	                    layout: {pack: 'start'},
	                    action: 'voltarCustomersList',
	                    text: 'Clientes'
	                },	                
	        		{			        	       	
			        	xtype: 'button',
			        	action: 'novoPedido',
			        	docked: 'right',
			        	//text: 'Novo Pedido',
			        	iconCls: 'compose',
    					iconMask: true,
    					ui: 'confirm'
			        }			    	
	            ]
	        }, 
			{
                //flex: 2,
				xtype: 'list',
				store: 'Orders',
				disableSelection: true,	
				//onItemDisclosure: true,
				listeners: {
					itemtap: function(view, index, target, record, evt, eOpts ) {

						var controler = Ext.getCmp('App.controller.Customers');

						tapListCustomersForm(view, index, target, record, evt, eOpts);						
					}
				},
				id: 'listCustomersForm',
				plugins: {
			        xclass: 'Ext.plugin.SlideToRemove',
			        buttonWidth: '15%',
			        removeText: 'Deletar'
			    },
				/*
				items: [					
					{
						xtype: 'listitemheader',
						cls: 'light',
						html: 'Pedidos'
					}
				],*/
				itemTpl: new Ext.XTemplate(
		            '<div class="avatar" style="background-image: url(resources/img/{status}.png);width: 3%; max-width:3%; no-repeat auto"></div>',
		            '<div width = "100%">',		            
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 10%;word-wrap:break-word;max-width:10%; vertical-align:top;"><h4>Pedido EMS: </h4>{ordernum}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 15%;word-wrap:break-word;max-width:15%; vertical-align:top;"><h4>Pedido Cliente: </h4>{ordercust}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 13%;word-wrap:break-word;max-width:13%; vertical-align:top;"><h4>Data Emissão: </h4>{createdate}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 14%;word-wrap:break-word;max-width:14%; vertical-align:top;"><h4>Data Entrega: </h4>{deliverydate}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 15%;word-wrap:break-word;max-width:15%; vertical-align:top;"><h4>Valor Mercadoria: </h4>{[this.valueFormat(values.value)]}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 15%;word-wrap:break-word;max-width:15%; vertical-align:top;"><h4>Valor Total: </h4>{[this.valueFormat(values.valueEMS)]}</div>',
		            '<div align="center" style="display: inline-block;overflow: hidden;width: 15%;word-wrap:break-word;max-width:15%; vertical-align:top;"><h4>Situação EMS: </h4>{statusEMS}</div>',
		            {
		                compiled: true,
		                valueFormat: function(value) {

		                    if (Number(value)) {
		                        value = String(Number(value).toFixed(2)).replace('.', ',');
		                    }

		                    return value;
		                }
		            }		            
		        ),
			}
        ]
	}
});