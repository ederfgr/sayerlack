Ext.define('App.view.customer.CustomersCard', {

	extend: 'Ext.NavigationView',
	xtype: 'customersContainer',

	config: {
        optimizedItems: null,
		navigationBar: false,
        tab: {
			title: 'Clientes',
	        iconCls: 'user',	        
    		iconMask: true,
	        action: 'customersTab'
	    },

        autoDestroy: true,
        layout: {
        	animation: {
        		type: 'fade'
        	}
        },
		items: [
			/*{
	            docked: 'top',
	            xtype: 'toolbar',
	            ui   : 'light',
	            title: 'Clientes'
	        },*/
			{
				xtype: 'customerslist'
			}
		],
        listeners: {
            activate: function (me, newActiveItem, oldActiveItem, eOpts) {
                      
                var ordersStore = Ext.getStore('Orders');
           
                ordersStore.clearFilter();
           
                if ((custnum != 0) && (typeof custnum != 'undefined')) {
           
                ordersStore.filterBy(function(rec) {
                                return rec.get('custnum') === custnum;
                                });				
                }
           
                var items = me.getOptimizedItems();
           
                if (!items) {
                    return;
                }
                      
                me.add(items);
            } ,
            deactivate: function (tab){
                      
                tab.setOptimizedItems(tab.getActiveItem());
                tab.removeAll(false, true);
           
           }
        }
           
	}
});
