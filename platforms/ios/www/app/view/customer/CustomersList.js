Ext.define('App.view.customer.CustomersList', {  
    extend: 'Ext.List',  
    alias:'widget.customerslist',
    xtype: 'customerslist',  
    fullscreen: true,  
    id: 'customerslist', 
    ui: 'light',  

    config:{
        disableSelection:true,  
        store:'Customers',  
        title: 'Clientes' ,
        //onItemDisclosure: true,
        layout:{
            type: 'fit',
            animation: {
                type: 'fade'
            }
        },
        itemTpl: new Ext.XTemplate(
            '<div class="avatar" style="background-image: url(resources/img/customer.png);"></div>',
            '<h3 style=color:{[this.formatColor(values)]}>{name}</h3>',
            '<h4>Código: {code} CNPJ: {cnpj} Nome Abrev: {abrev_name} Email: {email} Endereço: {address} {bairro} {city} {uf} </h4>',
            {
                compiled: true,
                formatColor: function(custRecord) {

                    var dias = Ext.getStore("Settings").getAt(0).get("diasSemPedido");
                    
                    var pedidos = Ext.getStore("Orders");
                    var dataBase = (Ext.Date.add(new Date(), Ext.Date.DAY, dias * -1));                    
                    var achou = false;
                        
                    pedidos.each(function(record){
                        if(record.get('custnum') == custRecord.code ) {
                            if (Ext.Date.parse(record.get('deliverydate'),"d/m/Y") > dataBase ) {
                                achou = true;
                                return false;
                            }
                        }
                    });                    

                    if (achou)
                        return "black";
                    else
                        return "red";
                }
            }
        ),
        grouped: true,
                
        itemCls: 'customer',

        //itemTpl:'{code} {name}',  
        items:[{  
            xtype:'toolbar',                                       
            docked:'top',  
            items:[{  
                xtype: 'searchfield',                          
                itemId:'customer_search',  
                id:'customer_search',                         
                placeHolder: ' Buscar Clientes'                
            }]  
        }]
    }
});