Ext.define('App.view.Login', {
    extend: 'Ext.form.Panel',
    xtype: 'loginView',
           autoDestroy: true,
    config: {
        optimizedItems: null,
        layout: {
           type: 'vbox',
           pack: 'center',
           align: 'center'
        },
        scrollable: false,
        scroll: false,
        overflowY: 'auto',
        baseCls: 'panelBackground',
        items: [
            {
                xtype: 'fieldset',
                ui: '',                
                items: [
                    
                    {
                        xtype: 'textfield',
                        id: 'userlogin',
                        border: 1,
                        label: 'Usuário',
                        autoComplete: false,
                        autoCapitalize: false
                    },
                    {
                        xtype: 'passwordfield',
                        id: 'pwdlogin',
                        border: 1,
                        label: 'Senha'
                    },
                    {
                        xtype: 'button',
                        action: 'login',                        
                        ui: 'gray',
                        text: 'Acessar'
                    }
                ]
            }
            
        ],
           listeners: {
           activate: function (me, newActiveItem, oldActiveItem, eOpts) {
           var items = me.getOptimizedItems();
           
           if (!items) {
           return;
           }
           
           me.add(items);
           },
           deactivate: function(tab) {
           tab.setOptimizedItems(tab.getActiveItem());
           tab.removeAll(false, true);
           }
           }
    }

});
