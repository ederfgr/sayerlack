Ext.define('App.view.carriers.CarriersList', {  
    extend: 'Ext.List',  
    alias:'widget.carrierslist',
    xtype: 'carrierslist',  
    fullscreen: true,  
    id: 'carrierslist', 
    ui: 'light',  

    config:{
        disableSelection:true,  
        store:'Carriers',  
        title: 'Transportadoras' ,
        //onItemDisclosure: true,
        layout:{
            type: 'fit',
            animation: {
                type: 'fade'
            }
        },
        itemTpl: [            
            '<h3>{code} - {abrevName} </h3>',
            '<h4>{fullName}</h4>'
            
        ],
        grouped: true,
                
        //itemCls: 'customer',

        //itemTpl:'{code} {name}',  
        items:[{  
            xtype:'toolbar',                                       
            docked:'top',  
            items:[{  
                xtype: 'searchfield',                          
                itemId:'carriers_search',  
                id:'carriers_search',                         
                placeHolder: ' Buscar Transportadoras',
                listeners: {
                    keyup: function(field) {
                        var value = field.getValue(),  
                        store = Ext.getCmp('carrierslist').getStore();
                   
                        store.clearFilter();
                  
                        if (value) {  
                            var searches = value.split(' '),  
                            regexps = [],  
                            i;
                  
                            for (i = 0; i < searches.length; i++) {  
                                if (!searches[i]) continue;  
                  
                                regexps.push(new RegExp(searches[i], 'i'));  
                            }  
                  
                            store.filter(function(record) 
                            {  
                                var matched = [];
                                
                                value = value.toUpperCase();
                                 
                                didMatch = record.get('code').indexOf(value) != -1 ||
                                record.get('abrevName').indexOf(value) != -1 ||
                                record.get('fullName').indexOf(value) != -1 ;
                                 
                                matched.push(didMatch);
                                if (didMatch)
                                    return matched[0];
                  
                                for (i = 0; i < regexps.length; i++)
                                {  
                                    var  search = regexps[i],  
                                       didMatch = record.get('code').match(search) || record.get('abrevName').match(search) || record.get('fullName').match(search) ;
                  
                                    matched.push(didMatch);
                                }              
                  
                                if (regexps.length > 1 && matched.indexOf(false) != -1) 
                                {  
                                    return false;  
                                } 
                                else
                                {  
                                    return matched[0];  
                                }  
                            });  
                        }
                    },
                    clearicontap: function () {
                        Ext.getCmp('carrierslist').getStore().clearFilter();  
                    }
                }
            }]  
        }],
        listeners: {
            itemtap: function(view, index, target, record, evt, opts) {
                Ext.getCmp('carrier').setValue(record.get("abrevName"));
                Ext.getCmp('carriersPopup').destroy();

            }
        }
    }
});
