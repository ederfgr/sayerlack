Ext.define('App.view.Main', {

	extend: 'Ext.tab.Panel',
	xtype: 'main',

	config: {

		tabBarPosition: 'bottom',
		tabBar: {
			ui: 'light'
		},		
		items: [
			{ xclass: 'App.view.customer.CustomersCard' },
			{ xclass: 'App.view.order.OrdersCard' },			
			{ xclass: 'App.view.support.SupportCard' },
			{ xclass: 'App.view.settings.SettingsCard' }
		]
	}
});