Ext.define('App.view.settings.SettingsForm',{
    
    extend: 'Ext.form.Panel',
    alias: 'widget.settingsform',
    store: 'Settings',    
    config: {
        layout: {
            type: 'fit',
            animation: {
                type: 'fade'
            } 
        },
        margin: '0px',
        //fullscreen: true,
        items: [
            {
                docked: 'top',
                xtype: 'toolbar',
                ui   : 'light',
                title: 'Parâmetros',
            },
            {               
                xtype: 'panel',
                id: 'settingspanel',                
                flex: 1,
                layout: 'hbox',
                docked: 'top',
                margin: '0px 15px',
                items: [
                    {
                        xtype: 'fieldset',                       
                        title: '',    
                        instructions: 'Configurações atualizadas automaticamente na sincronização do dispositivo',
                        width: '100%',                       
                        //layout: {pack: 'start'},
                        style: "margin: 0px",
                        defaults: {
                            //required: true,
                            labelAlign: 'left',
                            labelWidth: '40%'
                        },
                        items: [            
                            {
                                xtype: 'urlfield',
                                name: 'urlws',
                                id: 'urlwsfield',
                                label: 'URL Webservice',
                                disabled: true,
                                autoCapitalize: false
                            },
                            {
                                xtype: 'urlfield',
                                name: 'urldownload',
                                id: 'urldownloadfield',
                                label: 'URL Download',
                                disabled: true,
                                autoCapitalize: false
                            },                            
                            {
                                xtype: 'textfield',
                                id: 'usernamefield',
                                name: 'username',
                                disabled: true,
                                label: 'Usuário'
                            }
                        ]
                    }
                ],
                listeners:{
                    painted: function(panel, eOpts) {

                        var form = Ext.getCmp('settingspanel');

                        var settingsStore = Ext.getStore('Settings');

                        var urlws = Ext.getCmp('urlwsfield'),
                            urldown = Ext.getCmp('urldownloadfield'),
                            username = Ext.getCmp('usernamefield');

                        urlws.setValue(settingsStore.getAt(0).data.urlws);
                        urldown.setValue(settingsStore.getAt(0).data.urlDownload);
                        username.setValue(settingsStore.getAt(0).data.username);
                    }
                }
            }
                ]
    }
});
