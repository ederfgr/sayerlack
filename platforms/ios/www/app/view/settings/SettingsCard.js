Ext.define('App.view.settings.SettingsCard', {

    extend: 'Ext.NavigationView',
    xtype: 'settingsContainer',

    config: {
        optimizedItems: null,
        
        navigationBar: false,
        tab: {
            title: 'Parâmetros',
            iconCls: 'settings',
            iconMask: true
        },
        layout: {
            animation: {
                type: 'fade'
            }
        },

        autoDestroy: false,
        items: [
            {
				xtype: 'settingsform'
			}
                ],
           listeners: {
           activate: function (me, newActiveItem, oldActiveItem, eOpts) {
           var items = me.getOptimizedItems();
           
           if (!items) {
           return;
           }
           
           me.add(items);
           },
           deactivate: function(tab) {
           tab.setOptimizedItems(tab.getActiveItem());
           tab.removeAll(false, true);
           }
           }
    }
});