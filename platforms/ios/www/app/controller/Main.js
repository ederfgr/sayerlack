Ext.require(['App.controller.Sql']);
Ext.require(['App.controller.Support']);

var didSync = false;
var countTransp = '';

Ext.define('App.controller.Main', {
	extend: 'Ext.app.Controller',
	config: {
		refs: {
			customersContainer: 'customersContainer',
            mainPage: {
                xtype: 'main',
                selector: 'main',
                autoCreate: true
            },
            loginView: 'loginView',

            syncContainer: 'syncContainer'
			
		},

		control: {
			'button[action=login]' : {
				tap: 'doLogin'
			},
			'card[action=customersTab]' : {
				tap: 'switchCardCustomerts'
			}
		}
	},

	showCustomersList: function(){
		Ext.Viewport.setActiveItem(this.getCustomersList());
	},

	doLogin: function() {
           
        console.log('doLogin');

		Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Aguarde...' });

		var settings = Ext.getStore('Settings');

		var me = this;
           
		settings.load(function(records, operation, success) {

			var settings = this;
                      
			//var settingsRecord = settings.getAt(0);

			try {
				urlws = records[0].get('urlws');
			}
			catch (err){
                Ext.Msg.alert('ErroLogin', err.message, Ext.emptyFn);
                Ext.Viewport.setMasked(false);
			}

			storedUsername = records[0].get('username');
	        storedPassword = records[0].get('password');
	        storedBloqipad = records[0].get('bloqIpad');
                      
            console.log(storedUsername);
                      
	        //Se não houver usuário salvo ou o acesso estiver bloqueado, a autenticação será online
	        if (!storedUsername || storedBloqipad === "yes") {
                      
                Ext.Ajax.request({

                    //console.log(urlws);
                                 
		            url: urlws + "doLogin.p",
		            method: 'post',
		            disableCaching : 'true',
		            params: {
		                username: username,
		                password: password
		            },
		            success: function (response) {
		                try	{
		                	var loginResponse = Ext.JSON.decode(response.responseText);	
		                }
		                catch (err) {	                	
		                	me.signInFailure(err.message);
		                	return;
		                }
                        
                        console.log(loginResponse.message);
                                 
		                if (loginResponse.success === "true") {  
		                	try	{	  	                		         		
		                		records[0].set('username',username);
		                		records[0].set('password',password);
		                		records[0].set('bloqIpad','no');		                		
		                		records[0].dirty = true;

		                		settings.sync();
		                		
		                	}                  
		                	catch(err) {
		                		Ext.Msg.alert('Alerta', "Login não armazenado: " + err.message , Ext.emptyFn);
		                	}
                                 
                            
		                    	                    
		                    me.signInSuccess();
		                }
		                else {
		                    me.signInFailure(loginResponse.message);
		                }
		            },
		            failure: function (response) {                
		                me.signInFailure('Login falhou. Tente novamente mais tarde.');
		            }
		        });
		    }
		    else{
		    	if (storedUsername != username || storedPassword != password) {
		    		me.signInFailure("Usuário ou senha incorretos");
		    	}
		    	else{
		    		me.signInSuccess();
		    	}
		    }
		}, this);
           
		username = Ext.util.Format.trim(this.getLoginView().down("#userlogin").getValue());
		password = this.getLoginView().down("#pwdlogin").getValue();		

		//console.log('Username: ' + username + '\n' + 'Password: ' + password);
           
		if (username.length === 0 || password.length === 0) {
           
            //console.log('Please enter your username and password.');

            Ext.Viewport.setMasked(false);

            return;
        }        
    }, 
    signInSuccess: function () {
        console.log('Signed in.');
           console.log('before getMainPage');
        Ext.Viewport.add({ xtype: 'main'});
        console.log('before getMainPage');
        Ext.Viewport.setActiveItem(this.getMainPage());
        
        Ext.Viewport.setMasked(false);
        
        console.log('before ordersStore');
           
        var me = this,
        	ordersStore = Ext.getStore('Orders');
           
           console.log('after ordersStore');
           
        //Testar conexão - Phonegap
        Ext.apply(Ext.MessageBox.YESNO, [{text: 'Não',itemId: 'no', ui: 'action'},{text: 'Sim', itemId: 'yes'}]);
           
        Ext.Msg.confirm("Sincronizar", "Deseja atualizar o cadastro de clientes, transportadoras, pedidos e tabela de preço?",
        	function ( answer ) {
                        
        		console.log(answer);
                        
        		if ( answer == 'yes') {
                        
                    var online = checkConnection();
                        
                    if (online == 'No network connection')
                        Ext.Msg.alert("Erro Conexão!", "iPad sem conexão, favor verificar", Ext.emptyFn);
                    else {
                        
                        console.log('ATUALIZAR');
                        
                        Ext.Viewport.setMasked({ xtype: 'loadmask', message:'Atualizando dados. Aguarde...' });
                        
                        me.getParams();
                        
                        console.log('FIM ATUALIZAR');
                    }
        		}
                else {
                    var online = checkConnection();
                        
                    if (online != 'No network connection') {
                        var supportController = App.app.getController('Support');
                        
                        supportController.downloadFile('Promocao.jpg');
                    }
                }
                
        	}
        );
           
        var settings = Ext.getStore('Settings');
		var settingsRecord = settings.getAt(0);				
		
        ordersStore.sort({
			property: 'orderipad',
			sorterFn: function(rec1, rec2) {
				return (Number(rec1.get('orderipad')) > Number(rec2.get('orderipad'))) ? 1 : (Number(rec1.get('orderipad')) === Number(rec2.get('orderipad')) ? 0 : -1);
			}
		});
           
		
		
        settingsRecord.set('lastAcc', new Date());
           
		ordersStore.sort('custnum', 'ASC');
           
        contSql.update (db, 'settings',
            settingsRecord.data,
            'id="1"',
            'AND',
            function (results) {
                console.log('gravou settings');
            },
            function(err) {
                //console.log(err.message);
            });
           
        this.getSiteList();
           
        this.getCarriers();
           
        this.getFileList();
           
        onOnline();
        
    },
    signInFailure: function (message) {
        //console.log('Erro Login:' + message);
           
           console.log('fail1');

        Ext.Msg.alert('Erro', message, Ext.emptyFn);
           
           console.log('fail2');

        Ext.Viewport.setMasked(false);
           
           console.log('fail3');
    },
    /*************************************************************************
     ** Para otimizar o espaço no banco de dados não foi criada uma tabela **
     ** para armazenar apenas os estabelecimentos que o usuário tem acesso **
     ** para isso foi criado o campo 'sitelist' na tabela settings, onde os**
     ** estabelecimentos são armazenados separados por ',' (virgula)       **
     ************************************************************************/

    getSiteList: function (){
           
        var settings = Ext.getStore('Settings');
        var settingsRecord = settings.getAt(0);

        var siteList = settingsRecord.get('sitelist');

        var sites = siteList.split(',');
           
        var data = [];
        for (var i = 0; i < sites.length; i++) {
            data.push({
                text: sites[i],
                value: sites[i]
            });            
        };

        Ext.getStore('SiteList').setData(data);
           
    },
    getParams: function () {
           
        var me = this;
           
    	Ext.Ajax.request({
            url: urlws + "getParams.p",
            useDefaultXhrHeader: false,
            method: 'post',
            disableCaching : 'true',
            params: {
                username: username
            },
            success: function (response) {
                try	{
                	loginResponse = Ext.JSON.decode(response.responseText);	
                }
                catch (err) {	                	
                	Ext.Msg.alert('Erro no servidor', err.message, Ext.emptyFn);
                	Ext.Viewport.setMasked(false);
                	return;
                }

                //var contSql = App.app.getController('Sql');

                getSettings(
                	loginResponse,
                	function (resultSettings) {

                		var settings = Ext.getStore('Settings');

                		if (resultSettings == "OK")
                			settings.load();
                            

		        		var payRecords = loginResponse.params[1].paymentterms;

                            
		        		if (payRecords && payRecords.length > 0) {

                            console.log("########");
		        			var paymentterms = Ext.getStore('PaymentTerms');    

			        		paymentterms.removeAll();
			        		paymentterms.sync();

			        		for (var i = payRecords.length - 1; i >= 0; i--) {

			        			payRec = payRecords[i];

			        			getPayment(
			        				payRec,
			        				i,
			        				function (results, cont) {

			        					if (results == "OK")
			        						paymentterms.load();

			        					if (cont == 0) {
			        						var typeRecords = loginResponse.params[2].orderType;

							        		if (typeRecords.length > 0) {

							        			var orderTypeStore = Ext.getStore('OrderType');    

								        		orderTypeStore.removeAll();
								        		orderTypeStore.sync();

								        		for (var i = typeRecords.length - 1; i >= 0; i--) {
								        			typeRec = typeRecords[i];

								        			getOrderType(
								        				typeRec,
								        				i,
								        				function (results, cont) {
								        					if (results == "OK")
																orderTypeStore.load();

															if (cont == 0) {
                                                                 me.getSiteList();
                                                                 
                                                                 me.getPriceTables();

															}
								        				}
								        			);
								        		};
							        		};							        		
			        					}
		                         	}
			        			);
			        		};
		        		};
                 	}
                );
            },
            failure : function() {
                 Ext.Msg.alert('Erro no servidor', response.responseText, Ext.emptyFn);
                 Ext.Viewport.setMasked(false);
		    }
        });
    },
    getCustomers: function (){

    	var settings = Ext.getStore('Settings');
		var settingsRecord = settings.getAt(0);	

		urlws = settingsRecord.get('urlws');
		username = settingsRecord.get('username');

    	var customersStore = Ext.getStore('CustomersRemote');

    	customersStore.getProxy().setUrl(urlws + "getCustomers.p");
    	//customersStore.getProxy().setUseDefaultXhrHeader(false);

    	customersStore.getProxy().addListener('exception', function (proxy, response, operation) {
	      	Ext.Msg.alert('Erro no servidor', response.responseText, Ext.emptyFn);
	      	Ext.Viewport.setMasked(false);
	    });

	    var me = this;    	
    	customersStore.load({
		    params:{
		        username: username
		    },
		    callback : function(records, operation, success) {
		        //console.log("Total Registros: " + records.length);
		        //console.log("Sucesso: " + success);
                            
		        if (success) {

	        		if (records.length > 0) {
	        			var store2 = Ext.getStore('Customers');		
			        	var ordersStore = Ext.getStore('Orders'); 
						var itemsStore = Ext.getStore('OrderItems');

						Ext.getStore('CustomerPaymentTerms').removeAll();
                        Ext.getStore('CustomerPaymentTerms').sync();       

                        store2.removeAll();
                        store2.sync();

			        	ordersStore.clearFilter();
                            
                            
                        ordersStore.sort({
                                         property: 'orderipad',
                                         sorterFn: function(rec1, rec2) {
                                            return (Number(rec1.get('orderipad')) > Number(rec2.get('orderipad'))) ? 1 : (Number(rec1.get('orderipad')) === Number(rec2.get('orderipad')) ? 0 : -1);
                                        }
                        });
                            
                            //console.log('teste1');
                            
                        var nextOrder = 1;
                            
                            //console.log('teste2');
                            
                        if (ordersStore.last())
                            nextOrder = ordersStore.last().get('orderipad') + 1;
                            
                        //console.log('teste3');
                            
                        //console.log('nextOrder - ' + nextOrder);
                            
			        	ordersStore.filter('status', 'Integrado');

			        	var orders = ordersStore.getData();
                            
                        //console.log('orders.length - ' + orders.length);

			        	if (orders.length > 0) {
			        		for (var i = orders.length - 1; i >= 0; i--) {
			        			var ordRec = orders.items[i];
                                //console.log(ordRec.data.ordernum);
                                //console.log(ordRec.data.status);

			        			getDeleteItem(
			        				ordRec,
			        				i,
			        				function (results, cont, pedido) {
			        					if (results == "OK")
							        		ordersStore.remove(pedido);

										if (cont == 0) {

											/*var nextOrder = 0;
                                              
                                            if(ordersStore.last())
						                        nextOrder = Number(ordersStore.last().get('orderipad') + 1);
											else
												nextOrder = 1;
                                              
                                            console.log('nextOrder - ' + nextOrder);*/

											var cust = customersStore.getData();

											customersStore.destroy();

											for (var k = cust.length - 1; k >= 0; k--) {
						                            
												record = cust.items[k];

											  	store2.add(record.copy());

											  	//Adicionar pedidos

											  	var pedidos = record.raw.orders;
                                                var CPTS = record.raw.customerpaymentterms;
                                              
                                                if (CPTS.length > 0) {
                                                	Ext.getStore('CustomerPaymentTerms').removeAll();
                        							Ext.getStore('CustomerPaymentTerms').sync();

                                                    for (var i = CPTS.length - 1; i >= 0; i--) {
                                                        cpt = CPTS[i];
                                                        newRec = Ext.create('App.model.CustomerPaymentTerms', {
                                                              custnum : record.raw.code,
                                                              paymentTerm: cpt.code,
                                                              financialIndex :  cpt.financialIndex
                                                        });
                                              
                                                        //var contSql = App.app.getController('Sql');
                                              
                                                        contSql.insert (db, 'customerpaymentterms',
                                                            newRec.data,
                                                            function (results) {
                                                                
                                                            },
                                                            function(err) {
                                                                console.log("erro ao inserir CP cliente: " + err);
                                                            }
                                                        );
                                                    }                                                    

                                                }

											  	if (pedidos.length > 0 ) {

												  	for (var i = pedidos.length - 1; i >= 0; i--) {
												  		order = pedidos[i];
                                              
												  		newRec = Ext.create('App.model.Orders', {
														   	custnum : order.custnum,
														   	orderipad: nextOrder,
														   	ordernum :  order.ordernum,
														   	custname : order.custname,
														   	createdate : order.createdate,
												        	siteid : order.siteid,
												        	deliverydate : order.deliverydate,
												        	discount : order.discount,
												        	ordertype : order.ordertype,
												        	paymentTerms: order.paymentTerms,
												        	remarks : order.remarks,
												            value : order.value,
												            status : order.status,
												            statusEMS : order.statusEMS,
												            ordercust :  order.ordercust,
														   	valueEMS: order.valueEMS,
														   	carrier: order.carrier
														});

												  		nextOrder ++;

												  		var itemsOrder = order.orderitems;

												  		getNewOrder(
												  			newRec,
												  			itemsOrder,
												  			k,
												  			function (results, cont, itemsOrder, newOrder) {
									        					if (results == "OK") {

									        						for (var i = itemsOrder.length - 1; i >= 0; i--) {
									        							items = itemsOrder[i];

									        							var newItem = Ext.create('App.model.OrderItems', {
																  			orderipad : newOrder.data.orderipad,
																		   	itemcode : items.itemcode,
																		   	description : items.description,
																        	un : items.un,
																        	quantity : items.quantity,
																        	value : items.value,
										                                    discount : items.discount
																		});

																		var today = new Date();

																		newItem.set('id', newItem.get('id') + today.getMilliseconds());

																		getNewItem(
																			newItem,
																			cont,
																			i,
																			function (results, contPed, contItem ) {

																				//console.log('cont ped - ' + contPed);
																				//console.log('cont item - ' + contItem);

													        					/*if (results == "OK")
																					itemsStore.load();*/

																				if (contPed == 0 && contItem == 0) {
																					//console.log('fim inclusao pedidos');
																					//ordersStore.sync();
                                                                                   
                                                                                   if (!didSync) {
                                                                                        store2.sync();
                                                                                        didSync = true;
                                                                                   }
                                                                                   

																					var itemStore  = Ext.getStore('OrderItems'),
															                            supportStore = Ext.getStore('Support');

															                        var ordersStore = Ext.getStore('Orders'); 

															                        ordersStore.load();
															                        ordersStore.sort('custnum');
															                        itemStore.load();
															                        supportStore.load();
															                               
															                        var settings = Ext.getStore('Settings');
															                        var settingsRecord = settings.getAt(0);
															                               
															                        //var contSql = App.app.getController('Sql');
															                               
															                        if (ordersStore.last())
															                            settingsRecord.set('lastorder', (Number(ordersStore.last().get('orderipad')) + 1));
															                               
															                        contSql.update (db, 'settings',
												                                        settingsRecord.data,
												                                        'id="1"',
												                                        'AND',
												                                        function (results) {
												                                        	Ext.getStore('CustomerPaymentTerms').load();
                                                                                            Ext.getCmp('customerslist').refresh();
												                                        	Ext.Viewport.setMasked(false);
                                                                                            var supportController = App.app.getController('Support');
                                                                                            supportController.downloadFile('Promocao.jpg');
												                                        },
												                                        function(err) {
												                                        	Ext.Viewport.setMasked(false);
                                                                                                    //window.location.reload();
                                                                                                    //console.log('joao 2');

												                                       	 	//console.log(err.message);
															                        	}
															                        );
																				}
													        				}
																		);
									        						};
									        					}
									        				}
												  		);
												  	};
												}
												else {
													if (k == 0) {
				                                       	store2.sync();

				                                       	var itemStore  = Ext.getStore('OrderItems'),
								                        	supportStore = Ext.getStore('Support');

								                        //var ordersStore = Ext.getStore('Orders'); 

								                        ordersStore.load();
								                        ordersStore.sort('custnum');
								                        itemStore.load();
								                        supportStore.load();                                                                       
													       
							                        	var settings = Ext.getStore('Settings');
							                        	var settingsRecord = settings.getAt(0);
							                               
							                        	//var contSql = App.app.getController('Sql');
							                               
							                        	if (ordersStore.last())
							                            	settingsRecord.set('lastorder', (Number(ordersStore.last().get('orderipad')) + 1));
							                               
								                        contSql.update (db, 'settings',
					                                        settingsRecord.data,
					                                        'id="1"',
					                                        'AND',
					                                        function (results) {
					                                        	Ext.getStore('CustomerPaymentTerms').load();
					                                            Ext.getCmp('customerslist').refresh();
					                                        	Ext.Viewport.setMasked(false);
                                                                var supportController = App.app.getController('Support');
                                                                supportController.downloadFile('Promocao.jpg');
					                                        },
					                                        function(err) {
					                                        	Ext.Viewport.setMasked(false);
								                        	}
								                        );
													}

												}

											}
										}
			        				}
								);
			        		};
			        	} else {
			        		/*var nextOrder = 0;

				        	if(ordersStore.last())
		                        nextOrder = Number(ordersStore.last().get('orderipad') + 1);
							else
								nextOrder = 1;*/

							var cust = customersStore.getData();

							customersStore.destroy();

							for (var k = cust.length - 1; k >= 0; k--) {
		                            
								record = cust.items[k];

							  	store2.add(record.copy());

							  	var CPTS = record.raw.customerpaymentterms;
                                              
                                if (CPTS.length > 0) { 
                                	                                        	
                                    for (var i = CPTS.length - 1; i >= 0; i--) {
                                        cpt = CPTS[i];
                                        newRec = Ext.create('App.model.CustomerPaymentTerms', {
                                              custnum : record.raw.code,
                                              paymentTerm: cpt.code,
                                              financialIndex :  cpt.financialIndex
                                        });
                              
                                        //var contSql = App.app.getController('Sql');
                              
                                        contSql.insert (db, 'customerpaymentterms',
                                            newRec.data,
                                            function (results) {
                                                
                                            },
                                            function(err) {
                                                console.log("erro ao inserir CP cliente: " + err);
                                            }
                                        );
                                    }
                                }
                            
                                //console.log(record.raw.code);

							  	//Adicionar pedidos
							  	var pedidos = record.raw.orders;

							  	if (pedidos.length > 0) {

								  	for (var i = pedidos.length - 1; i >= 0; i--) {
								  		order = pedidos[i];

								  		newRec = Ext.create('App.model.Orders', {
										   	custnum : order.custnum,
										   	orderipad: nextOrder,
										   	ordernum :  order.ordernum,
										   	custname : order.custname,
										   	createdate : order.createdate,
								        	siteid : order.siteid,
								        	deliverydate : order.deliverydate,
								        	discount : order.discount,
								        	ordertype : order.ordertype,
								        	paymentTerms: order.paymentTerms,
								        	remarks : order.remarks,
								            value : order.value,
								            status : order.status,
								            statusEMS : order.statusEMS,
								            ordercust :  order.ordercust,
										   	valueEMS: order.valueEMS,
										   	carrier: order.carrier
										});

								  		nextOrder ++;

								  		var itemsOrder = order.orderitems;

								  		getNewOrder(
								  			newRec,
								  			itemsOrder,
								  			k,
								  			function (results, cont, itemsOrder, newOrder) {
					        					if (results == "OK") {

					        						for (var i = itemsOrder.length - 1; i >= 0; i--) {
					        							items = itemsOrder[i];

					        							var newItem = Ext.create('App.model.OrderItems', {
												  			orderipad : newOrder.data.orderipad,
														   	itemcode : items.itemcode,
														   	description : items.description,
												        	un : items.un,
												        	quantity : items.quantity,
												        	value : items.value,
						                                    discount : items.discount
														});

														var today = new Date();

														newItem.set('id', newItem.get('id') + today.getMilliseconds());

														getNewItem(
															newItem,
															cont,
															i,
															function (results, contPed, contItem) {

																//console.log('cont ped - ' + contPed);
																//console.log('cont item - ' + contItem);

									        					//if (results == "OK")
																	

																if (contPed == 0 && contItem == 0) {
																	//itemsStore.load();
																	//console.log('fim inclusao pedidos');
																	//ordersStore.sync();
                                                                   if (!didSync) {
                                                                       store2.sync();
                                                                       didSync = true;
                                                                   }

																	var itemStore  = Ext.getStore('OrderItems'),
											                            supportStore = Ext.getStore('Support');

											                        var ordersStore = Ext.getStore('Orders'); 

											                        ordersStore.load();
											                        ordersStore.sort('custnum');
											                        itemStore.load();
											                        supportStore.load();
											                               
											                        var settings = Ext.getStore('Settings');
											                        var settingsRecord = settings.getAt(0);
											                               
											                        //var contSql = App.app.getController('Sql');
											                               
											                        if (ordersStore.last())
											                            settingsRecord.set('lastorder', (Number(ordersStore.last().get('orderipad')) + 1));
											                               
											                        contSql.update (db, 'settings',
								                                        settingsRecord.data,
								                                        'id="1"',
								                                        'AND',
								                                        function (results) {

								                                        	Ext.getStore('CustomerPaymentTerms').load();
                                                                            Ext.getCmp('customerslist').refresh();
								                                        	Ext.Viewport.setMasked(false);
                                                                            
                                                                            var supportController = App.app.getController('Support');
                                                                            supportController.downloadFile('Promocao.jpg');
								                                        },
								                                        function(err) {
								                                        	Ext.Viewport.setMasked(false);
                                                                                    //window.location.reload();
                                                                                    //console.log('joao 4');

								                                       	 	//console.log(err.message);
											                        	}
											                        );

																	//me.getPriceTables();
																}
									        				}
														);
					        						};
					        					}
					        				}
								  		);
								  	};
								}
								else {
									if (k == 0) {
                                       	store2.sync();

                                       	var itemStore  = Ext.getStore('OrderItems'),
				                        	supportStore = Ext.getStore('Support');

				                        var ordersStore = Ext.getStore('Orders'); 

				                        ordersStore.load();
				                        ordersStore.sort('custnum');
				                        itemStore.load();
				                        supportStore.load();                                                                       
									       
			                        	var settings = Ext.getStore('Settings');
			                        	var settingsRecord = settings.getAt(0);
			                               
			                        	//var contSql = App.app.getController('Sql');
			                               
			                        	if (ordersStore.last())
			                            	settingsRecord.set('lastorder', (Number(ordersStore.last().get('orderipad')) + 1));
			                               
				                        contSql.update (db, 'settings',
	                                        settingsRecord.data,
	                                        'id="1"',
	                                        'AND',
	                                        function (results) {
	                                        	Ext.getStore('CustomerPaymentTerms').load();
	                                            Ext.getCmp('customerslist').refresh();
	                                        	Ext.Viewport.setMasked(false);
                                                var supportController = App.app.getController('Support');
                                                supportController.downloadFile('Promocao.jpg');
	                                        },
	                                        function(err) {
	                                        	Ext.Viewport.setMasked(false);
				                        	}
				                        );
									}

								}
							}

			        	}
		        	}	
		        }
		        else {
		        	Ext.Msg.alert('Erro', 'Não foi possível conectar ao servidor', Ext.emptyFn);
		        	Ext.Viewport.setMasked(false);
		        }
		        	
		    },
		    failure : function() {
	    		Ext.Viewport.setMasked(false);
		    }
		});
    },
    getPriceTables : function() {

        var settings = Ext.getStore('Settings');
		var settingsRecord = settings.getAt(0);	

		urlws = settingsRecord.get('urlws');
		
    	var priceRemoteStore = Ext.getStore('PriceTablesRemote');

    	priceRemoteStore.getProxy().setUrl(urlws + "getPriceTables.p");
    	//priceRemoteStore.getProxy().setUseDefaultXhrHeader(false);

    	priceRemoteStore.getProxy().addListener('exception', function (proxy, response, operation) {
	      	Ext.Msg.alert('Erro no servidor', response.responseText, Ext.emptyFn);
	      	Ext.Viewport.setMasked(false);
	    });

    	var me = this;   

    	priceRemoteStore.load({
		    callback : function(records, operation, success) {
		        //console.log("Total Registros: " + records.length);
		        //console.log("Sucesso: " + success);
		        if (success) {

		        	
		        	deletePriceTable(
		        		function (results) {
        					if (results == "OK") {
        						var storePrice = Ext.getStore('PriceTables');

							  	storePrice.load();

							  	var prices = priceRemoteStore.getData();

							  	if (prices) {
					        		for (var i = prices.length - 1; i >= 0; i--) {
					        			priRec = prices.items[i];

					        			var newTable = Ext.create('App.model.PriceTables', {
										   	//id : record.data.id,
										   	code :  priRec.data.code,
										   	codeItem : priRec.data.codeItem,
								        	un : priRec.data.un,
								        	price : priRec.data.price
										});

										getNewTable(
											newTable,
											i,
											function (results, cont) {
                                                    
												if (results == "OK") 
													storePrice.load();

                                                    
												if (cont == 0) {

                                                    me.getCustomers();

												}
											}
										);
					        		}
					        	}

        					}
        				}
		        	);
		        }
		        else {

		        	Ext.Msg.alert('Erro', 'Não foi possível conectar ao servidor', Ext.emptyFn);
		        	Ext.Viewport.setMasked(false);
		        }
		        	
		    },
		    failure : function() {
	    		Ext.Viewport.setMasked(false);
		    }
		});
    },
    getFileList: function () {
           
        var me = this;

    	Ext.Ajax.request({
            url: urlws + "getSupportFilesList.p",
            useDefaultXhrHeader: false,
            method: 'post',
            disableCaching : 'true',
            params: {
                username: username
            },
            success: function (response) {
                try	{
                	var files = Ext.JSON.decode(response.responseText);	
                }
                catch (err) {	                	
                	Ext.Msg.alert('Erro no servidor', err.message, Ext.emptyFn);
                	return;
                }   

                if (files.files.length > 0) {
                	var store = Ext.getStore('Support');	        	
		        	store.removeAll();
		        	store.sync();	

		        	for (var i = files.files.length - 1; i >= 0; i--) {
		        		file = files.files[i];
                         
                        getFile(
                        	file,                        	
                            function (file,isUpdated) {
                                newRec = Ext.create('App.model.Support', {
                                	filename : file.filename,
                                    filetype :  file.filetype,
                                    description : file.desc,
                                    uptodate : isUpdated
                            	});
                                
                                /*
                                if(file.filename == "Promocao.jpg" && isUpdated == "Nao") {
                                    var path = docDir.fullPath + '/Promocao.jpg';
                                    
                                    window.open("file://" + path, '_blank', 'hidden=no,enableViewportScale=yes,allowInlineMediaPlayback=yes,location=no');
                                }*/

                                me.getApplication().getController('Sql').insert (db, 'support',
                                	newRec.data,
                                    function (results) {
                                    	store.load();

                                    	//console.log('gravou arquivos');
                                    },
                                    function(err) {
                                    	//console.log(err.message);
                                   	}
                                );
                         	}
                        );
                        
                         //docDir.getFile(file.filename,{create:false}, function(f) {f.getMetadata(metadataFile,onFileError);}, onFileError);
		        	};
                };
            },
            failure : function() {
                 Ext.Msg.alert('Erro no servidor', response.responseText, Ext.emptyFn);
                 Ext.Viewport.setMasked(false);
		    }
        });
    },
    getCarriers: function () {
           
        var me = this;
        var settings = Ext.getStore('Settings');
        var settingsRecord = settings.getAt(0);

        urlws = settingsRecord.get('urlws');

        Ext.Ajax.request({
            url: urlws + "getCarriers.p",
            useDefaultXhrHeader: false,
            method: 'post',
            disableCaching : 'true',
            params: {
                username: username
            },
            success: function (response) {
                try    {
                    var carriers = Ext.JSON.decode(response.responseText);
                }
                catch (err) {
                    Ext.Msg.alert('Erro no servidor', err.message, Ext.emptyFn);
                    return;
                }

                if (carriers.carriers.length > 0) {
                    var store = Ext.getStore('Carriers');
                    var sql = me.getApplication().getController('Sql');
                    //store.removeAll();
                    sql.query (db, 'delete from carriers;',
                        function (results) {},
                        function(err) {}
                    );
                    
                    countTransp = carriers.carriers.length;
                    
                    for (var i = carriers.carriers.length - 1; i >= 0; i--) {
                        carrier = carriers.carriers[i];
                        
                        newRec = Ext.create('App.model.Carriers', {
                            code : carrier.code,
                            abrevName :  carrier.abrevName,
                            fullName : carrier.fullName
                        });
                        
                        sql.insert (db, 'carriers',
                            newRec.data,
                            function (results) {
                                
                                if (results.insertId == countTransp) {
                                    
                                    store.load();
                                }
                            },
                            function(err) {
                                console.log(err.message);
                            }
                        );
                    };
                };
            },
            failure : function() {
                 Ext.Msg.alert('Erro no servidor', response.responseText, Ext.emptyFn);
                 Ext.Viewport.setMasked(false);
            }
        });
    }

})

function readFile(file, callback) {
    
    var fileSource = docDir.fullPath + "/" + file.filename;
    var isUpdated = "Sim";
    
    var fileExists = function(fileEntry){
        
        var entry = fileEntry;
        
        console.log('Existe - ' + entry.name);
        
        function success(metadata) {
//            console.log("Last Modified: " + metadata.modificationTime);
//            console.log("Data Arquivo:  " + file.docDate);
            
            var auxDate = file.docDate.split('/');
            
            var docDate = new Date(Number(auxDate[2]),(Number(auxDate[1]) - 1),Number(auxDate[0]));
            
//            console.log('modTime - ' + metadata.modificationTime.getTime());
//            console.log('docDate - ' + docDate.getTime());
            
//            console.log('maior - ' + (docDate.getTime() >= metadata.modificationTime.getTime()));
            
            if (docDate.getTime() >= metadata.modificationTime.getTime()) {
//                console.log('callback-sim - ' + file.filename);
                callback(file,"Sim");
            } else {
//                console.log('callback-nao - ' + file.filename);
                callback(file,"Nao");
            }
        }
        
        function fail(error) {
            //console.log('callback-sim-fail - ' + file.filename);
            callback(file,"Sim");
            alert(error.code);
        }
        
//        console.log('Existe - ' + entry.name);
        
        // Request the metadata object for this entry
        entry.getMetadata(success, fail);
        
        //callback(file,"Nao");
    }
    
    var fileDoesNotExist = function() {
        callback(file,"Sim");
    }
    
    docDir.getFile(file.filename, {create: false}, fileExists, fileDoesNotExist);
    
}

function getFile(file, callback) {
    var success = function(file,isUpdated) {
        if ( typeof(callback) == 'function' ) callback(file,isUpdated);
    }
    readFile(file, success);
}

function saveSettings(loginResponse, callback)  {

	var settings = Ext.getStore('Settings');
	var settingsRecord = settings.getAt(0);
	//var contSql = App.app.getController('Sql');

	settingsRecord.set('sitelist',loginResponse.params[0].sitelist);
	settingsRecord.set('bloqIpad',loginResponse.params[3].bloqIpad);
	settingsRecord.set('minValue',loginResponse.params[5].minValue);
	settingsRecord.set('minMsg',loginResponse.params[6].minMsg);

	if (loginResponse.params[4].urlws != "")
		settingsRecord.set('urlws',loginResponse.params[4].urlws);
             
	if (loginResponse.params[7].urlDownload != "")
		settingsRecord.set('urlDownload',loginResponse.params[7].urlDownload);
    
    /*
    settingsRecord.set('urlws','https://pedidos1.sayerlack.com.br:8091/scripts/cgiip.exe/WService=emsweb-teste/service/');
    settingsRecord.set('urlDownload','https://pedidos1.sayerlack.com.br:8091/pdweb/apoio/');
    */
    
        
    settingsRecord.set('diasSemPedido',loginResponse.params[8].diasSemPedido);

	var settings = Ext.getStore('Settings');

	contSql.update (db, 'settings',
        settingsRecord.data,
        'id="1"',
        'AND',
        function (results) {
            callback("OK");            
        },
        function(err) {
        	callback("ERROR");  
        }
    );
}

function getSettings(loginResponse, callback) {

	var success = function(results) {
		if (typeof(callback) == 'function' ) callback(results);
	}

	saveSettings(loginResponse, success);
}

function savePayment(payRec, callback) {
	//var contSql = App.app.getController('Sql');
	
	contSql.insert (db, 'paymentterms',
		payRec,
		function (results) {
			callback("OK");			
		},
		function(err) {
			callback("ERROR");
		}
	);
}

function getPayment(payRec, cont, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont);
	}

	savePayment(payRec, success);
}

function saveOrderType(typeRec, callback) {
	//var contSql = App.app.getController('Sql');

	contSql.insert (
		db, 
		'ordertype',
		typeRec,
		function (results) {
			callback("OK");			
		},
		function(err) {
			callback("ERROR");
		}
	);
}

function getOrderType(typeRec, cont, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont);
	}

	saveOrderType(typeRec, success);
}

function deleteItem(ordRec, callback) {
	
	//var contSql = App.app.getController('Sql');
    
    //console.log("vai eliminar pedido: " + ordRec.data.ordernum + "-" + ordRec.data.orderipad);
    
    contSql.query (db,'delete from orders where orderipad="' + ordRec.data.orderipad + '"',
        function (results) {
            
             
        },
        function(err) {
            //console.log("erro ao eliminar pedido:" + err);
        }
    );

	contSql.query (db,'delete from orderitems where orderipad="' + ordRec.data.orderipad + '"',
		function (results) {
            //console.log("eliminou item pedido: "+ ordRec.data.ordernum);
			callback("OK");
		},
		function(err) {
            //console.log("erro ao eliminar item do pedido:" + err);
			callback("ERROR");
		}
	);
}

function getDeleteItem(ordRec, cont, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont, ordRec);
	}

	deleteItem(ordRec, success);
}

function addNewOrder(newRec, callback) {
	//var contSql = App.app.getController('Sql');

	contSql.insert (db, 'orders',
		newRec.data,
		function (results) {		
			callback("OK");
		},
		function(err) {
			callback("ERROR");
            console.log(err);
		}
	);
}

function getNewOrder(newRec, orderItens, cont, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont, orderItens, newRec);
	}

	addNewOrder(newRec, success);
}

function addNewItem(newItem, callback) {
	//var contSql = App.app.getController('Sql');

	contSql.insert (db, 'orderitems',
		newItem.data,
		function (results) {
			callback("OK");
		},
		function(err) {
			callback("ERROR");
            console.log(err);
		}
	);
}

function getNewItem(newRec, cont, contPed, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont, contPed);
	}

	addNewItem(newRec, success);
}

function deleteTableSql(callback) {

	//var contSql = App.app.getController('Sql');

	contSql.query (db, 'delete from pricetables',
		function (results) {
			callback("OK");			
		},
		function(err) {
			callback("ERROR");			
		}
	);

}

function deletePriceTable(callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results);
	}

	deleteTableSql(success);
}

function addNewTable(newRec, callback) {

	//var contSql = App.app.getController('Sql');

	contSql.insert (db, 'pricetables',
		newRec.data,
		function (results) {
			callback("OK");			
		},
		function(err) {
			callback("ERROR");			
		}
	);
}

function getNewTable(newRec, cont, callback) {

	var success = function (results) {
		if (typeof(callback) == 'function' ) callback(results, cont);
	}

	addNewTable(newRec, success);
}
