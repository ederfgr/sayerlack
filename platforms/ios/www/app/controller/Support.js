//window.onerror = function(err,fn,ln) {console.log("ERROR:" + err + ", " + fn + ":" + ln);};

Ext.define('App.controller.Support', {
    extend: 'Ext.app.Controller',

    config: {
        refs: {
            supportContainer: 'supportContainer',
            supportList: {
                main: 'supportContainer',
                selector: 'supportlist'
            }
        },

        control: {
            supportList: {
                itemtap: 'supportFileTap'
            }
        }
    },

    supportFileTap: function (view, index, target, record, evt, opts) {

        var me = this;
        if (record && record.data) {
            var filename = record.get("filename");
            //baixar nova versão do servidor
            if (record.get("uptodate") == "Sim") {
                Ext.Msg.confirm("Download", "O download desse arquivo pode levar alguns minutos, confirma?",
                    function (answer) {
                        if (answer == 'yes') {
                            me.downloadFile(filename);

                        } else {
                            filepath = docDir.fullPath + "/" + filename;

                            window.open("file://" + filepath, '_blank', 'hidden=no,enableViewportScale=yes,allowInlineMediaPlayback=yes,location=no');
                            //window.plugins.childBrowser.showWebPage("file://" + filepath,{ showLocationBar: true, ShowAddress: false });
                        }

                    }
                );

            } else {
                filepath = docDir.fullPath + "/" + filename;

                var rect = document.body.getBoundingClientRect();

                var opts = {
                    viewHeight: rect.height,
                    viewWidth: (rect.width * 0.3), // width of side menu
                    viewPosX: 0,
                    viewPosY: 0
                };

                ExternalFileUtil.openWith(
                    filepath,
                    "com.adobe.pdf",
                    opts,
                    function (res) {
                        console.log("ExternalFileUtil success");
                    },
                    function (res) {
                        hutch.error.showDialogue("Could not open PDF", "Please make sure you have a PDF viewer installed, e.g., Adobe Reader. Search for 'PDF Reader' in the App Store.");
                    });


                //window.open("file://" + filepath, '_blank', 'hidden=no,enableViewportScale=yes,allowInlineMediaPlayback=yes,location=no');
                //window.plugins.childBrowser.showWebPage("file://" + filepath,{ showLocationBar: true , showAddress: false});
            }
        }
    },
    downloadFile: function downloadFile(filename) {
        var settings = Ext.getStore('Settings');
        var settingsRecord = settings.getAt(0);

        var urlDl = settingsRecord.get('urlDownload');

        console.log('urldownload:' + urlDl);

        var uri = encodeURI(urlDl + filename);

        console.log("getdir" + docDir);

        var fileTransfer = new FileTransfer();
        //           console.log(typeof fileTransfer);

        filepath = docDir.fullPath + "/" + filename;

        console.log(uri);

        //Ext.Viewport.setMasked({ xtype: 'loadmask', message:'Aguarde, baixando arquivo...' });

        fileTransfer.onprogress = function (progressEvent) {
            if (progressEvent.lengthComputable) {
                Ext.Viewport.setMasked({
                    xtype: 'loadmask',
                    message: Number((progressEvent.loaded / progressEvent.total) * 100).toFixed(2) + "%"
                });
                //console.log('download progress: '+ (progressEvent.loaded / progressEvent.total) * 100 + "%");
            }
        };

        //           console.log('Download 1');

        Ext.Viewport.setMasked({
            xtype: 'loadmask',
            message: 'Baixando arquivo...'
        });

        //           console.log('Download 2');

        fileTransfer.download(
            uri,
            filepath,
            function (entry) {
                console.log("download complete: " + entry.fullPath);
                Ext.Viewport.setMasked(false);

                window.open("file://" + entry.fullPath, '_blank', 'hidden=no,enableViewportScale=yes,allowInlineMediaPlayback=yes,location=no');

                //window.plugins.childBrowser.showWebPage("file://" + entry.fullPath,{ showLocationBar: true, showAddress: false });

                if (filename != "Promocao.jpg")
                    setUpdated(filename);
            },
            function (error) {
                //Ext.Viewport.setMasked(false);
                console.log("download error source " + error.source);
                console.log("download error target " + error.target);
                console.log("upload error code" + error.code);
                //console.log(http_status);
                Ext.Viewport.setMasked(false);
            },
            true, {
                headers: {
                    "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                }
            }
        );
    }

});

function setUpdated(filename) {
    //var contSql = App.app.getController('Sql');

    var support = Ext.getStore('Support');
    var record = support.findRecord("filename", filename, 0, false, false, true);

    record.set('uptodate', "Nao");

    //support.save();
    support.sync();

    support.load();

}
