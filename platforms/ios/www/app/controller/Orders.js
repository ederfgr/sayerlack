var auxNewValue = 0, newValue = 0 , afterShow = false;

Ext.define('App.controller.Orders', {
	extend: 'Ext.app.Controller',

	config: {
		refs: {
			ordersContainer: 'ordersContainer',
			ordersList: {
				main: 'ordersContainer',
				selector: 'orderslist'
			},			
			ordersForm: {
				main: 'ordersContainer',
				xtype: 'ordersform',
				selector: 'ordersform',
				autoCreate: true
			},
			itemsGroupsList: {
				main: 'ordersForm',
				xtype: 'itemsgroupslist',
				selector: 'itemsgroupslist',
				autoCreate: true
			},
			addItemForm: {
				main: 'ordersForm',
				xtype: 'additemform',
				selector: '#additemform',
				autoCreate: true
			},
			subGroupsContainer: {
				main: 'itemsGroupsList',
				selector: '#subGroupsContainer'
			},
			subGroupsDataView: '#subGroupsDataView'
			,
			toolbarOrdersForm: '#toolbarOrdersForm'	,
			listItemsGroups: '#listItemsGroups',
			listOrderItems: '#listOrderItems'
		},

		control: {			
	        ordersList: {
				itemtap: 'showOrdersForm'
			},
			subGroupsDataView : {
				itemtap: 'showAddItemForm'
			},			
			'button[action=voltarOrdersList]' : {
				tap: 'showOrdersList'
			},
			'button[action=voltarOrderForm]' : {
				tap: 'backOrderForm'
			},
			'button[action=addItem]' : {
				tap: 'addItem'
			},
			'button[action=voltarItemsGroupsList]' : {
				tap: 'backGroup'
            },
            'button[action=sendMail]' : {
                tap: 'sendMail'
            },
            'button[action=copyOrder]' : {
                tap: 'copyOrder'
            },
			'button[action=saveOrder]' : {
				tap: 'salvarPedido'
			},
			'button[action=deleteOrder]' : {
				tap: 'deletarPedido'
			},
			'button[action=syncOrder]' : {
				tap: 'checkOrder'
			},
			'itemsgroupslist searchfield[itemId=items_search]' : {
	            clearicontap :  'onSearchClearIconTapItem',
	            keyup: 'onSearchKeyUpItem'
	        },
	        'orderslist searchfield[itemId=orders_search]' : {
	        	clearicontap :  'onSearchClearIconTapOrder',
	            keyup: 'onSearchKeyUpOrder'
	        },
	        'button[action=salvarItens]' : {
				tap: 'salvarItens'
			},
			listOrderItems: {
				itemtap: 'updateItem'
			}	        
		}
	},
	onSearchKeyUpItem: function(field) {  
		
        //get the store and the value of the field  
        
        var value = field.getValue(),  
        store = Ext.getCmp('subGroupsDataView').getStore();
  
        //first clear any current filters on thes tore  
        store.clearFilter();
  
        //check if a value is set first, as if it isnt we dont have to do anything  
        if (value) {  
            //the user could have entered spaces, so we must split them so we can loop through them all  
            var searches = value.split(' '),  
            regexps = [],  
            i;  
  
            //loop them all  
            for (i = 0; i < searches.length; i++) {  
                //if it is nothing, continue  
                if (!searches[i]) continue;  
  
                //if found, create a new regular expression which is case insenstive  
                regexps.push(new RegExp(searches[i], 'i'));  
            }  
  
            //now filter the store by passing a method  
            //the passed method will be called for each record in the store  
            store.filter(function(record) 
            {  
                var matched = [];
                
                value = value.toLowerCase();
                didMatch = record.get('code').toLowerCase().indexOf(value) != -1 ||
                           record.get('desc').toLowerCase().indexOf(value) != -1 ||
                           record.get('attr1').toLowerCase().indexOf(value) != -1 ||
                           record.get('attr2').toLowerCase().indexOf(value) != -1 ||
                           record.get('attr3').toLowerCase().indexOf(value) != -1 ||
                           record.get('attr4').toLowerCase().indexOf(value) != -1
                         ;
                matched.push(didMatch);
                if (didMatch)
                    return matched[0];
  
                //loop through each of the regular expressions  
                for (i = 0; i < regexps.length; i++)
                {  
                    var  search = regexps[i],  
            		   didMatch = record.get('code').match(search) ||
                                  record.get('desc').match(search) ||
                                  record.get('attr1').match(search) ||
                                  record.get('attr2').match(search) ||
                                  record.get('attr3').match(search) ||
                                  record.get('attr4').match(search) 
                         ;  //if it matched the first or last name, push it into the matches array
  
                    matched.push(didMatch);
            	}  //if nothing was found, return false (dont so in the store)                 
  
	            if (regexps.length > 1 && matched.indexOf(false) != -1) 
	            {  
	                return false;  
	            } 
	            else
	            {  
	            	//else true true (show in the store)  
	                return matched[0];  
	            }  
            });  
        }
    },  
  
    /** 
     * Called when the user taps on the clear icon in the search field. 
     * It simply removes the filter form the store 
     */  
    onSearchClearIconTapItem: function() {  

        //call the clearFilter method on the store instance  
        Ext.getCmp('subGroupsDataView').getStore().clearFilter();
    },

    onSearchKeyUpOrder: function(field) {  
		
        //get the store and the value of the field  
        
        var value = field.getValue(),  
        store = Ext.getCmp('orderslist').getStore();
  
        //first clear any current filters on thes tore  
        store.clearFilter();
  
        //check if a value is set first, as if it isnt we dont have to do anything  
        if (value) {  
            //the user could have entered spaces, so we must split them so we can loop through them all  
            var searches = value.split(' '),  
            regexps = [],  
            i;  
  
            //loop them all  
            for (i = 0; i < searches.length; i++) {  
                //if it is nothing, continue  
                if (!searches[i]) continue;  
  
                //if found, create a new regular expression which is case insenstive  
                regexps.push(new RegExp(searches[i], 'i'));  
            }  
  
            //now filter the store by passing a method  
            //the passed method will be called for each record in the store  
            store.filter(function(record) 
            {  
                var matched = [];
                
                value = value.toUpperCase();
                 
                didMatch = record.get('ordernum').indexOf(value) != -1 || record.get('custnum').indexOf(value) != -1 || record.get('ordercust').indexOf(value) != -1 || record.get('custname').indexOf(value) != -1 ;
                 
                matched.push(didMatch);
                if (didMatch)
                   return matched[0];
  
                //loop through each of the regular expressions  
                for (i = 0; i < regexps.length; i++)
                {  
                    var  search = regexps[i],  
            		   didMatch = record.get('ordernum').match(search) || record.get('custnum').match(search) || record.get('ordercust').match(search) || record.get('custname').match(search);  //if it matched the first or last name, push it into the matches array
  
                    matched.push(didMatch);
            	}  //if nothing was found, return false (dont so in the store)                 
  
	            if (regexps.length > 1 && matched.indexOf(false) != -1) 
	            {  
	                return false;  
	            } 
	            else
	            {  
	            	//else true true (show in the store)  
	                return matched[0];  
	            }  
            });  
        }
    },  
  
    /** 
     * Called when the user taps on the clear icon in the search field. 
     * It simply removes the filter form the store 
     */  
    onSearchClearIconTapOrder: function() {  

        //call the clearFilter method on the store instance  
        Ext.getCmp('orderslist').getStore().clearFilter();
    },

	showAddItemForm: function(view, index, target, record, evt, opts){

		record = view.getStore().getAt(index);
		var line = record.data.code;
		
		//var form = this.getOrdersForm();

		var buttonBack = Ext.getCmp('btBackItemsGroup');

		if (buttonBack)
			buttonBack.setDisabled(false);

		var categStore = Ext.getStore('ItemsGroups');

		var categoria = categStore.getData().findBy(function(categs) {
			return categs.get('code') === record.get('codeGroup');
		});
           
		var orderType = Ext.getStore('Orders').getData().items[0];
           
		var orderAux = Number(orderType.get('ordertype'));
           
		if (orderAux == 94 || orderAux == 40 || orderAux == 89)
			orderAux = 01;
           
		if (Number(categoria.get('orderType')) == orderAux) {
           
			var viewmask = Ext.Viewport;

			var colorStore = Ext.getStore('Colors');

			colorStore.clearFilter(true);
           
			colorStore.filter('lineCode', line);
           
	    	this.getOrdersContainer().setActiveItem(this.getAddItemForm(), { type: 'slide', direction: 'left' });
           
		} else {
           			
			if (orderType.get('ordertype') != "30" && orderType.get('ordertype') != "22")
				Ext.Msg.alert('Erro Item', '"Tipo Pedido" do Item difere do tipo do Pedido', Ext.emptyFn);
			else {
           
                Ext.Viewport.setMasked(true);
           
				var colorStore = Ext.getStore('Colors');

				colorStore.clearFilter(true);

				console.log("111 " + line);

				colorStore.filter('lineCode', line);

		    	this.getOrdersContainer().setActiveItem(this.getAddItemForm(), { type: 'slide', direction: 'left' });
			}
		}
           
        //Ext.Viewport.setMasked(false);
	},
	backGroup: function() {
        Ext.getCmp('btBackItemsGroup').setDisabled(true);
           
        Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Aguarde...' });
           
		this.salvarItens();
	},
	addItem: function(){
           		
        var lineStore = Ext.getStore('Lines');

		lineStore.clearFilter();
           
		lineStore.load();
           
		if (this.getListItemsGroups() != undefined) {
			this.getListItemsGroups().deselectAll();
			this.getListItemsGroups().refresh();
		}
                      		
    	this.getOrdersContainer().setActiveItem(this.getItemsGroupsList(), { type: 'slide', direction: 'left' });           
	},

    showOrdersList: function(){

    	Ext.getStore('Orders').clearFilter();
           
        Ext.getCmp('statusbutton').setPressedButtons(4);
           
    	this.getOrdersContainer().setActiveItem(this.getOrdersList());
	},

	backOrderForm: function(){
    	this.getOrdersContainer().setActiveItem(this.getOrdersForm());
	},

    showOrdersForm: function(view, index, target, record, evt, opts)
    {
    	var itemsStore = Ext.getStore('OrderItems'),
    		custStore = Ext.getStore('Customers'),
    		form = this.getOrdersForm();
		
        if(record && record.data)
		{
			if (record.get('status') == "Liberado" || record.get('status') == "Integrado") {
				Ext.getCmp('btAddItem').setDisabled(true);
				Ext.getCmp('btSaveOrder').setDisabled(true);
				Ext.getCmp('btDeleteOrder').setDisabled(true);
				Ext.getCmp('btSyncOrder').setDisabled(true);
				form.setDisabled(true);				
			} else {
				Ext.getCmp('btAddItem').setDisabled(false);
				Ext.getCmp('btSaveOrder').setDisabled(false);
				Ext.getCmp('btDeleteOrder').setDisabled(false);
				Ext.getCmp('btSyncOrder').setDisabled(false);
				form.setDisabled(false);				
			}
           
			Ext.getCmp('paymentTerms').reset();
			Ext.getCmp('fdOrderType').reset();
           
			form.setRecord(record);
           
			var listItems = Ext.getCmp('listOrderItems');
			var orderlist = Ext.getCmp('orderslist');

			var customer =  custStore.getData().findBy(function(cust) {
				return cust.get('code') === record.get('custnum');
			});
           
            //this.getToolbarOrdersForm().setTitle(customer.get('code') + ' - ' + customer.get('name'));
            this.getToolbarOrdersForm().setTitle('<div style="font-size: 14px;">' + customer.get('code') + ' - ' + customer.get('name') + '</div>');

			Ext.getCmp('ordernum').disable();
			//Ext.getCmp('ordercust').disable();
			Ext.getCmp('valueTot').disable();
			Ext.getCmp('valueMerc').disable();

			Ext.getCmp('financialIndex').disable();
           
			var createDate = Ext.getCmp('createdate');

			createDate.setValue(Ext.Date.parse(record.data.createdate,'d/m/Y'));

			createDate.disable();		

			var deliveryDate = Ext.getCmp('deliverydate');	

			deliveryDate.setValue(Ext.Date.parse(record.data.deliverydate,'d/m/Y'));
           
			Ext.getCmp('listOrderItems').setHidden(false);
			Ext.getCmp('titleBarItem').setHidden(false);
           
            this.getOrdersContainer().setActiveItem(this.getOrdersForm(), { type: 'slide', direction: 'right' });
           
            //Fechar o teclado
            Ext.Function.defer(function () {
                document.activeElement.blur();
            },500);
		}
		else {
           
			form.setRecord(null);

			var toolBar = this.getToolbarOrdersForm();
           
			Ext.getCmp('ordercust').reset();			
			Ext.getCmp('valueMerc').reset();			
			Ext.getCmp('discount').reset();
			Ext.getCmp('valueTot').reset();			
			Ext.getCmp('ordernum').reset();
			Ext.getCmp('deliverydate').reset();
			Ext.getCmp('createdate').reset();			
			Ext.getCmp('remarks').reset();

			var payField = Ext.getCmp('paymentTerms'),
				typeField = Ext.getCmp('fdOrderType'),
				carrierField = Ext.getCmp('carrier');

			payField.reset();
			typeField.reset();
            typeField.setDisabled(false);
			carrierField.reset();
           
			var customer =  custStore.getData().findBy(function(cust) {
				return cust.get('code') === custnum;
			});
           
            //this.getToolbarOrdersForm().setTitle(customer.get('code') + ' - ' + customer.get('name'));

            this.getToolbarOrdersForm().setTitle('<div style="font-size: 18px;">' + customer.get('code') + ' - ' + customer.get('name') + '</div>');
           
			payField.setValue(customer.get('paymentTerm'));
			typeField.setValue(customer.get('ordertype'));
			carrierField.setValue(customer.get('carrier'));

			Ext.getCmp('listOrderItems').setHidden(true);
			Ext.getCmp('titleBarItem').setHidden(true);

			//Ext.getCmp('btAddItem').setDisabled(false);
			Ext.getCmp('btSaveOrder').setDisabled(false);
			Ext.getCmp('btDeleteOrder').setDisabled(true);
			Ext.getCmp('btSyncOrder').setDisabled(true);
			form.setDisabled(false);

			Ext.getCmp('ordernum').setDisabled(true);
			Ext.getCmp('ordercust').setDisabled(false);
			Ext.getCmp('valueMerc').setDisabled(true);
			Ext.getCmp('valueTot').setDisabled(true);
			Ext.getCmp('createdate').setDisabled(true);
			Ext.getCmp('financialIndex').setDisabled(true);
           
            this.getOrdersContainer().setActiveItem(this.getOrdersForm(), { type: 'slide', direction: 'right' });
           
		}

           
		//this.getOrdersContainer().setActiveItem(this.getOrdersForm(), { type: 'slide', direction: 'left' });
	},

	salvarPedido: function()
	{
		var form 	    = this.getOrdersForm(),
			ordersStore	= Ext.getStore('Orders'),
            custStore   = Ext.getStore('Customers'),
			settings    = Ext.getStore('Settings').getData().first();

		var view, index, target, evt, opts;

		//var contSql = App.app.getController('Sql');
           
        ordersStore.clearFilter();
           
        ordersStore.sort({
            property: 'orderipad',
            sorterFn: function(rec1, rec2) {
            return (Number(rec1.get('orderipad')) > Number(rec2.get('orderipad'))) ? 1 : (Number(rec1.get('orderipad')) === Number(rec2.get('orderipad')) ? 0 : -1);
            }
        });
           
        lastorder = ordersStore.last().get('orderipad');

		var orders, createDay, createMonth, createYear, deliveryDay, deliveryMonth, deliveryYear;

		if (form.getRecord() != null){
            console.log("#### 1");
            var record = ordersStore.getData().findBy(function(rec) {
                return rec.get('orderipad') == form.getRecord().getId()
            });
           
			record.set(form.getValues());

			createDay = form.getValues().createdate.getDate();
			createMonth = form.getValues().createdate.getMonth() + 1;
			createYear = form.getValues().createdate.getFullYear ();

			if (createMonth < 10)
				createMonth = "0" + createMonth;

			if (createDay < 10)
				createDay = "0" + createDay;

			deliveryDay = form.getValues().deliverydate.getDate();
			deliveryMonth = form.getValues().deliverydate.getMonth() + 1;
			deliveryYear = form.getValues().deliverydate.getFullYear ();

			if (deliveryMonth < 10)
				deliveryMonth = "0" + deliveryMonth;

			if (deliveryDay < 10)
				deliveryDay = "0" + deliveryDay;

			record.data.createdate = createDay + '/' + (createMonth) + '/' + createYear;
			record.data.deliverydate = deliveryDay + '/' + (deliveryMonth) + '/' + deliveryYear;
           
           
            if (record.get('value') == null)
                record.set('value', '0');
           
            if (record.get('valueEMS') == null)
                record.set('valueEMS', '0');
           
            console.log(record.get('orderipad'));
           
           var itemsStore = Ext.getStore('OrderItems');
           itemsStore.data.setFilters(null);
           
           itemsStore.filterBy(function(rec) {
                               return rec.data.orderipad == record.get('orderipad');
                               });
           
           var value = 0;
           itemsStore.each(function(recItem) {
                auxValue = Number(recItem.get('value')) * Number(recItem.get('quantity'));
                value = (value) + (auxValue - (auxValue * recItem.get('discount') / 100));
            });
           
           if(record && record.get('discount') > 0)
               value = value - (value * record.get('discount') / 100);
           
           Ext.getCmp('valueMerc').setValue(value);
           record.set('value', value);

           orders = record;

           contSql.update (db, 'orders',
                record.data,
                'orderipad="' + record.data.orderipad +'"',
                'AND',
                function (results) {
                    ordersStore.load();
                            
                    ordersStore.filterBy(function(orderRec) {
                        return orderRec.get('orderipad') == form.getRecord().getId();
                    });
                            
                	//Ext.Msg.alert("Pedido Salvo!", "Alteração efetuada com sucesso!", Ext.emptyFn);
                },
                function(err) {
                	Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
                });
		} else {
            console.log("#### 2");

			createDay = form.getValues().createdate.getDate();
			createMonth = form.getValues().createdate.getMonth() + 1;
			createYear = form.getValues().createdate.getFullYear();

			if (createMonth < 10)
				createMonth = "0" + createMonth;

			if (createDay < 10)
				createDay = "0" + createDay;

			deliveryDay = form.getValues().deliverydate.getDate();
			deliveryMonth = form.getValues().deliverydate.getMonth() + 1;
			deliveryYear = form.getValues().deliverydate.getFullYear();

			if (deliveryMonth < 10)
				deliveryMonth = "0" + deliveryMonth;

			if (deliveryDay < 10)
				deliveryDay = "0" + deliveryDay;

			var orderipad;
			
			settings.set('lastorder', (Number(lastorder) + 1));

			contSql.update (db, 'settings',
	            settings.data,
	            'id="1"',
	            'AND',
	            function (results) {
	                //console.log('gravou settings ultimo pedido' + orderipad);
	            },
	            function(err) {
	                //console.log(err.message);
	            }
	        );        
			//Ext.getStore('Settings').sync();
           
            var customer =  custStore.getData().findBy(function(cust) {
                                return cust.get('code') === custnum;
                            });
           
           
           
			orders = Ext.create('App.model.Orders', {
			   	custnum : custnum,
                custname: customer.get('code') + '-' + customer.get('name'),
			   	orderipad : Number(lastorder) + 1,
			   	ordercust :  form.getValues().ordercust,
			   	createdate : createDay + '/' + (createMonth) + '/' + createYear,
	        	siteid : form.getValues().siteid,
	        	deliverydate : deliveryDay + '/' + (deliveryMonth) + '/' + deliveryYear,
	        	discount : form.getValues().discount,	        	
	        	paymentTerms: form.getValues().paymentTerms,
	        	ordertype : form.getValues().ordertype,
	        	remarks : form.getValues().remarks,
	            value : form.getValues().value,
	            status : 'Pendente',
	            statusEMS : form.getValues().statusEMS,
	            carrier: form.getValues().carrier
			});
           
            form.setValues({orderipad : Number(lastorder) + 1});
           
            itemsStore = Ext.getStore('OrderItems');
           
            itemsStore.data.setFilters(null);
            itemsStore.filterBy(function(rec) {
                               return rec.data.orderipad == Number(lastorder) + 1;
                               });
			
	  		this.getApplication().getController('Sql').insert (db,'orders',
	  			orders.data,
	  			function (results) {
	  				Ext.getCmp('listOrderItems').setHidden(false);
	  				Ext.getCmp('titleBarItem').setHidden(false);
	  				ordersStore.load();
                                                               
                    ordersStore.filterBy(function(orderRec) {
                        return orderRec.get('orderipad') == orders.get('orderipad');
                    });
	  			},
	  			function(err) {
	  				Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
	  			});
		}		

		this.showOrdersForm(view, index, target, orders, evt, opts);
	},

	deletarPedido: function()
	{
		Ext.Msg.confirm(
			'Deletar pedido',
			'Tem certeza que deseja deletar este pedido?',
			function(btn){
				if(btn == 'yes'){

					var form 	    = this.getOrdersForm(),
						ordersStore	= Ext.getStore('Orders'),
						itemsStore	= Ext.getStore('OrderItems'),
						sqlControl  = this.getApplication().getController('Sql');

					if(form.getRecord() !== null){
						sqlControl.query (db,'delete from orders where orderipad="' + form.getRecord().data.orderipad + '"',
			        		function (results) {

			        			ordersStore.load();

			        			sqlControl.query (db,'delete from orderitems where orderipad="' + form.getRecord().data.orderipad + '"',
			        				function (results) {
			        					itemsStore.load();
			        					//Ext.Msg.alert("Pedido Eliminado!", "Pedido eliminado com sucesso!", Ext.emptyFn);
			        				},
			        				function(err) {
			        					Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
							  	})
		        			},
				  			function(err) {
				  				Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
					  	});

					  	this.showOrdersList();
					}
				}
			},
			this			
		);
	},
	salvarItens: function() {
           
        var ordersStore	= Ext.getStore('Orders');
        //ordersStore.clearFilter(true);
           
		var colorStore = Ext.getStore('Colors'),
			productStore = Ext.getStore('Products'),
			itemsStore = Ext.getStore('OrderItems'),
			lineStore = Ext.getStore('Lines'),
			codePai, colors, itemPedido,
			app = this,
			//order = ordersStore.getData().items[0],
			//order = ordersStore.getById(record.data.orderipad),
		 	form = this.getOrdersForm();
           
        newValue = 0,
        auxNewValue = 0;
           
        var colorCorre,
            order = form.getRecord(),
            correlatosStore = Ext.getStore('Correlatos'),
            //contSql = App.app.getController('Sql'),
            cores = colorStore.getData().items,
            auxContReg = 0, auxContResult = 0, auxCores = 0;
           
        for (var j = cores.length - 1; j >= 0; j--){
           
           //console.log('ini j - ' + j);
           
           auxCores = j;
           
           recColor = cores[j];
        
           codePai = recColor.data.code;
           
           getCodePai(
                codePai,
                j,
                function(results, colors, contCor) {
                      
                      auxContReg = Number(auxContReg) + Number(colors.length);
                      
                      for (var k = colors.length - 1; k >= 0; k--) {
                      
	                      //console.log('ini k - ' + k);
	                      rec = colors.items[k];
	                      
	                    if (rec.data.code.indexOf("/") != -1)
	                      	itemPedido = document.getElementById('__' + rec.data.code.split("/")[0]);
	                    else
	                    	itemPedido = document.getElementById('__' + rec.data.code);
	                      
	                      if (itemPedido) {
	                            getItemPedido(
	                                    itemPedido,
	                                    rec,
	                                    order,
	                                    colorCorre,
	                                    contCor,
	                                    k,
	                                    function (results, contCor, contItem, correAux, orderIpad, itemValue) {
	                                          
	                                        if (correAux)
	                                            colorCorre = correAux;
	                                          
	                                        auxContResult++;                                          
	                                          
	                                        if ((auxContResult == auxContReg) && auxCores == 0) {
	                                          
	                                            if (orderIpad == 'undefined') {
	                                                  lineStore.clearFilter();
	                                                  Ext.Viewport.setMasked(false);
	                                            } else {
	                                                if (colorCorre) {
	                                                    if (colorCorre.items.length > 0) {
	                                                        var correlatos = new Array();
	                                                        
	                                                        colorCorre.items.forEach(function(colorRecord) {
	                                                                                 correlatos.push(colorRecord.get('lineCorre'));
	                                                                                 });
	                                                        
	                                                        lineStore.clearFilter(true);
	                                                        
	                                                        lineStore.filterBy(function(lines) {
	                                                                           if (correlatos.indexOf(lines.get('code')) == -1)
	                                                                           return false;
	                                                                           else
	                                                                           return true;
	                                                                           });
	                                                    }
	                                                } else 
	                                                	lineStore.clearFilter();
	                                                
	                                                itemsStore.sync();
	                                                itemsStore.load();
	                                                
	                                                      
	                                                if (orderIpad) {
	                                                    itemsStore.data.setFilters(null);
	                                                    itemsStore.filter('orderipad', orderIpad);
	                                                }

	                                                var value = 0, priceItem = 0;
	                                             
	                                                itemsStore.each(function(recItem) {
	                                                	console.log(recItem);
                                                        priceItem = Number(recItem.get('value')).toFixed(2)
								  						auxValue = Number(recItem.get('value')).toFixed(2) * Number(recItem.get('quantity'));
									  					value = (value) + (auxValue - (auxValue * recItem.get('discount') / 100));
									  				});

	                                                order.set("value",value);
	                                          
	                                                contSql.update (db, 'orders',
	                                                                order.data,
	                                                                'orderipad="' + order.data.orderipad +'"',
	                                                                'AND',
	                                                                function (results) {
	                                                                    //console.log(results);
	                                                                
	                                                                    ordersStore.load();
	                                                                    form.setRecord(order);

	                                                                    var createDate = Ext.getCmp('createdate');

																		createDate.setValue(Ext.Date.parse(order.data.createdate,'d/m/Y'));

																		createDate.disable();		

																		var deliveryDate = Ext.getCmp('deliverydate');	

																		deliveryDate.setValue(Ext.Date.parse(order.data.deliverydate,'d/m/Y'));
	                                                                                                                                        
	                                                                    //app.getListItemsGroups().deselectAll();
	                                                                
	                                                                    if (colorCorre) {
	                                                                        if (colorCorre.items.length > 0) {

	                                                                        	app.getListItemsGroups().deselectAll();
	                                                                
	                                                                            Ext.Viewport.setMasked(false);

	                                                                            Ext.Msg.alert(
	                                                                                      'Indicações',
	                                                                                      'Os produtos apresentados a seguir são indicados no processo',
	                                                                                      function() {
	                                                                                          
	                                                                                      
	                                                                                      var correlatos = new Array();
	                                                                                      
	                                                                                      colorCorre.items.forEach(function(colorRecord) {
	                                                                                                               correlatos.push(colorRecord.get('lineCorre'));
	                                                                                                               });
	                                                                                      
	                                                                                      lineStore.clearFilter(true);
	                                                                                      
	                                                                                      lineStore.filterBy(function(lines) {
	                                                                                                         if (correlatos.indexOf(lines.get('code')) == -1)
	                                                                                                         return false;
	                                                                                                         else
	                                                                                                         return true;
	                                                                                                         });                                                                                                                                                                                
	                                                                                      }
	                                                                                      );
	                                                                        } else {
                                                                    
	                                                                        	if (codeGroup != null) {
	                                                                        		lineStore.clearFilter();

	                            													lineStore.filter('codeGroup', codeGroup);
	                            												}
                                                                                else
                                                                                    lineStore.clearFilter();

	                            												fldSearch = Ext.getCmp('items_search');	                            												

	                            												if (fldSearch.getValue().length > 0) {
	                            													fldSearch.fireEvent('keyup', fldSearch);
	                            												}

	                            											
	                                                                            Ext.Viewport.setMasked(false);
	                                                                        }
	                                                                    } else { 
	                                                                        if (codeGroup != null) {
                                                                        		lineStore.clearFilter();

                            													lineStore.filter('codeGroup', codeGroup);
                            												}
                                                                            else
                                                                                lineStore.clearFilter();

                            												fldSearch = Ext.getCmp('items_search');                            												

                            												if (fldSearch.getValue().length > 0) {
                            													fldSearch.fireEvent('keyup', fldSearch);
                            												}
	                                                                        Ext.Viewport.setMasked(false);
	                                                                    }	                                                                    
	                                                                    
	                                                                    app.getOrdersContainer().setActiveItem(app.getItemsGroupsList(), { type: 'slide', direction: 'left' });
	                                                                    
	                                                                    //app.getListItemsGroups().refresh();
	                                                                    },
	                                                                    function(err) {
	                                                                    //console.log("Erro Pedido!");
	                                                                    }
	                                                                );
	                                                }
	                                            }
	                                        }
	                                    );
	                      }
                      };
                  
                }
           );
		};
	},
	deletarItem: function(record)
	{
           
		var orderStore = Ext.getStore('Orders'),
			form 	   = this.getOrdersForm();

		var order = orderStore.getById(record.data.orderipad);

		var status = orderStore.getById(record.data.orderipad).get('status');

        if (status == 'Pendente' || status == 'Errosync') {
			/*Ext.Msg.confirm(
				'Deletar Item',
				'Tem certeza que deseja deletar este Item?',
				function(btn){
					if(btn == 'yes'){*/
           
			var itemsStore	= Ext.getStore('OrderItems'),
				sqlControl  = this.getApplication().getController('Sql');

			//var order = orderStore.getData().items[0];

			sqlControl.query (db,'delete from orderitems where id="' + record.get('id') + '"',
				function (results) {
                              
					itemsStore.remove(record);
					itemsStore.load();
					itemsStore.data.setFilters(null);
	  				itemsStore.filter('orderipad', record.get('orderipad'));

	  				Ext.getCmp('listOrderItems').refresh();

	  				if (itemsStore.getCount() != 0)
	  					Ext.getCmp('fdOrderType').setDisabled(true);
	  				else
	  					Ext.getCmp('fdOrderType').setDisabled(false);

	  				var itemsValue = itemsStore.getData();	  			

	  				var value = 0,
	  					auxValue = 0;

	  				itemsValue.items.forEach(function(recItem) {
  						auxValue = Number(recItem.get('value')) * Number(recItem.get('quantity'));
	  					value = Number(value) + (auxValue - (auxValue * recItem.get('discount') / 100));
	  				});

	  				//auxValue = Number(record.get('value')) * Number(record.get('quantity'));
	  				//value = Number(value) - (auxValue - (auxValue * record.get('discount') / 100));

	  				order.set('value', value);
                              
                    sqlControl.update (db, 'orders',
		                order.data,
		                'orderipad="' + order.get('orderipad') +'"',
		                'AND',
		                function (results) {
		                	//console.log("Pedido Salvo!");
		                	orderStore.load();
		                	form.setRecord(order);

		                	var createDate = Ext.getCmp('createdate');

							createDate.setValue(Ext.Date.parse(order.data.createdate,'d/m/Y'));

							createDate.disable();		

							var deliveryDate = Ext.getCmp('deliverydate');	

							deliveryDate.setValue(Ext.Date.parse(order.data.deliverydate,'d/m/Y'));

		                	//Ext.Msg.alert("Item Eliminado!", "Item eliminado com sucesso!", Ext.emptyFn);
		                },
		                function(err) {
		                	//console.log("Erro Pedido!");
		                }
	                );								
				},
				function(err) {
					Ext.Msg.alert("Erro Item", err.message, Ext.emptyFn);
				}
			)		        			
				/*	}
				},
				this			
			);*/
		}
	},
    sendMail: function() {
           var form 	    = this.getOrdersForm(),
               ordersStore	= Ext.getStore('Orders'),
               itemsStore   = Ext.getStore('OrderItems');
           
           var order = ordersStore.findRecord('orderipad', form.getRecord().data.orderipad);
           var jsonItems = new Array();
           
           var filters = itemsStore.getFilters();
           
           itemsStore.clearFilter(true);
           
           var paymentTermsStore = Ext.getStore('PaymentTerms'),
           financialValue = 0;
           
           var condPagto = paymentTermsStore.getData().findBy(function(rec) {
                                                              return rec.data.code == order.get('paymentTerms');
                                                              });
           
           itemsStore.each(function(items) {
                if (items.data.orderipad == order.data.orderipad) {
                           
                    var priceAux = 0;
                       
                    if (Ext.getCmp('btSaveOrder').isDisabled()) {
                    
                           priceAux = items.get('value');
                           
                    }
                    else {
                    
                           var priceTables = Ext.getStore('PriceTables').getData().findBy(function(tables){
                                                                                          return tables.get('codeItem').trim() == items.get('itemcode').trim();
                                                                                          
                                                                                          });
                           
                           if (priceTables != null)
                                priceAux = Number(priceTables.get('price')).toFixed(2);
                           
                           if (priceAux != Number(items.get('value'))) {
                                priceAux = Number(items.get('value'));
                           }
                           
                           
                           fldTaxa = Ext.getCmp('financialIndex').getValue();
                           
                           if (Number(fldTaxa) > 0)
                                priceAux = Number(priceAux) + (Number(priceAux) * Number(fldTaxa) / 100);
                           else {
                                if (Number(fldTaxa) < 0)
                                    priceAux = Number(priceAux) - ((Number(priceAux) * -1) * Number(fldTaxa) / 100);
                           }
                    }
                           
                    jsonItems.push(
                    {
                        orderipad: items.get('orderipad'),
                        itemcode: items.get('itemcode'),
                        un: items.get('un'),
                        quantity: items.get('quantity'),
                        discount: items.get('discount'),
                        value: priceAux
                    });
                }
           });
           
           itemsStore.setFilters(filters);
           
           Ext.Viewport.setMasked({ xtype: 'loadmask', message: 'Aguarde...' });
           
           Ext.Ajax.request({
                url: urlws + "printProposal.p",
                method: 'post',
                disableCaching : true,
                useDefaultXhrHeader: false,
                timeout: 300000,
                params: {
                    paymentTerms: order.get('paymentTerms'),
                    custnum: order.get('custnum'),
                    ordercust: order.get('ordercust'),
                    orderipad: order.get('orderipad'),
                    createdate: order.get('createdate'),
                    siteid: order.get('siteid'),
                    deliverydate: order.get('deliverydate'),
                    discount: order.get('discount'),
                    discountlist: order.get('discountlist'),
                    ordertype: order.get('ordertype'),
                    remarks: order.get('remarks'),
                    carrier: order.get('carrier'),
                    username: Ext.getStore('Settings').getAt(0).get('username'),
                    checkOrder: "true",
                    items: Ext.util.JSON.encode(jsonItems)
                },
                success: function (response) {
                    Ext.Viewport.setMasked(false);
                    try	{
                        var loginResponse = Ext.JSON.decode(response.responseText);
                    }
                    catch (err) {
                        Ext.Msg.alert('Erro no servidor', err.message, Ext.emptyFn);
                        console.log(err.message);
                        //me.signInFailure(err.message);
                        return;
                    }
                },
                failure : function() {                     
                     Ext.Viewport.setMasked(false);
                }
            });

    },
    /************************/
    copyOrder: function() {
    
        var form        = this.getOrdersForm(),
            ordersStore = Ext.getStore('Orders'),
            custStore   = Ext.getStore('Customers'),
            settings    = Ext.getStore('Settings').getData().first();
           
        var order = ordersStore.findRecord('orderipad', form.getRecord().data.orderipad);

        var view, index, target, evt, opts;
           
        ordersStore.clearFilter();
           
        ordersStore.sort({
            property: 'orderipad',
            sorterFn: function(rec1, rec2) {
            return (Number(rec1.get('orderipad')) > Number(rec2.get('orderipad'))) ? 1 : (Number(rec1.get('orderipad')) === Number(rec2.get('orderipad')) ? 0 : -1);
            }
        });
           
        lastorder = ordersStore.last().get('orderipad');

        //var orders, createDay, createMonth, createYear, deliveryDay, deliveryMonth, deliveryYear;
        
        var orderipad;
           
        settings.set('lastorder', (Number(lastorder) + 1));

        contSql.update (db, 'settings',
            settings.data,
            'id="1"',
            'AND',
            function (results) {
                //console.log('gravou settings ultimo pedido' + orderipad);
            },
            function(err) {
                //console.log(err.message);
            }
        );
       
        var customer =  custStore.getData().findBy(function(cust) {
                            return cust.get('code') === order.get('custnum');
                        });
        
        var today = new Date();
        var createDate = '';
        createDate = today.getDate().toString().padStart(2,'0') + '/' +
                        (today.getMonth() + 1).toString().padStart(2,'0') + '/' + today.getFullYear();
        
        newOrder = Ext.create('App.model.Orders', {
            custnum : order.get('custnum'),
            custname: customer.get('code') + '-' + customer.get('name'),
            orderipad : Number(lastorder) + 1,
            ordercust :  '',
            createdate : createDate,
            siteid : order.get('siteid'),
            deliverydate : createDate,
            discount : order.get('discount'),
            paymentTerms: order.get('paymentTerms'),
            ordertype : order.get('ordertype'),
            remarks : order.get('remarks'),
            value : order.get('value'),
            status : 'Pendente',
            //statusEMS : form.getValues().statusEMS,
            carrier: order.get('carrier')
        });
       
        form.setValues({orderipad : Number(lastorder) + 1});
       
        itemsStore = Ext.getStore('OrderItems');
       
        itemsStore.data.setFilters(null);
        itemsStore.filterBy(function(rec) {
                           return rec.data.orderipad == order.get('orderipad');
                           });
       
        contSql.insert (db,'orders',
            newOrder.data,
            function (results) {
                Ext.getCmp('listOrderItems').setHidden(false);
                Ext.getCmp('titleBarItem').setHidden(false);
                ordersStore.load();
                                                           
                ordersStore.filterBy(function(orderRec) {
                    return orderRec.get('orderipad') == newOrder.get('orderipad');
                });
            },
            function(err) {
                Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
            }
        );
        
        console.log ('order',order.get('orderipad'));
        console.log ('new order',newOrder.get('orderipad'));
        
        
        itemsStore.each(function(items) {
            if (items.data.orderipad == order.get('orderipad')) {
                console.log ('item',items.get('itemcode'));
                    
                var priceAux = 0;
                
                var priceTables = Ext.getStore('PriceTables').getData().findBy(
                    function(tables){
                        return tables.get('codeItem').trim() == items.get('itemcode').trim();
                    });
            
                if (priceTables != null)
                     priceAux = Number(priceTables.get('price')).toFixed(2);
            
                if (priceAux != Number(items.get('value')))
                    priceAux = Number(items.get('value'));
            
                /*fldTaxa = Ext.getCmp('financialIndex').getValue();
            
                if (Number(fldTaxa) > 0)
                    priceAux = Number(priceAux) + (Number(priceAux) * Number(fldTaxa) / 100);
                else {
                    if (Number(fldTaxa) < 0)
                        priceAux = Number(priceAux) - ((Number(priceAux) * -1) * Number(fldTaxa) / 100);
                }*/
                    
                newItem = Ext.create('App.model.OrderItems', {                
                    orderipad: newOrder.get('orderipad'),
                    itemcode: items.get('itemcode'),
                    description: items.get('description'),
                    un: items.get('un'),
                    quantity: items.get('quantity'),
                    discount: items.get('discount'),
                    value: priceAux
                });
                
                contSql.insert (db,'orderitems',
                    newItem.data,
                    function (results) {
                    },
                    function(err) {
                        Ext.Msg.alert("Erro Pedido!", err.message, Ext.emptyFn);
                    }
                );
            }
        });
        
        itemsStore.sync();
        itemsStore.load();
    
        this.showOrdersForm(view, index, target, newOrder, evt, opts);
    

    },
    /*******************************************************/
    
	checkOrder: function() {

		var form 	    = this.getOrdersForm(),
			ordersStore	= Ext.getStore('Orders');

		var settings = Ext.getStore('Settings').getData().first();

		var view, index, target, evt, opts;

		//var contSql  = App.app.getController('Sql'),
		var	contSync = App.app.getController('SyncOrder'),
			me = this;

		if (form.getRecord() != null){
			
			var order = ordersStore.findRecord('orderipad', form.getRecord().data.orderipad);
           
            if (order.get('ordertype') != 22 && order.get('ordertype') != 30 && order.get('ordertype') != 42 &&
                order.get('ordertype') != 43 && order.get('ordertype') != 82 && order.get('ordertype') != 83) {
           
           
                if (Number(order.get('value')) > Number(settings.get('minValue'))) {
				
                    order.set('status', 'Liberado');

                    contSql.update (db, 'orders',
                                    order.data,
                                    'orderipad="' + order.data.orderipad +'"',
                                    'AND',
                                    function (results) {
                                        //console.log("Pedido Salvo!");
                                        ordersStore.load();

                                        //Ext.Msg.alert('Pedido Liberado!','Pedido liberado para sincronizar com o EMS', Ext.emptyFn);

                                        contSync.syncPedido();

                                        me.showOrdersForm(view, index, target, order, evt, opts);
                                    },
                                    function(err) {
                                        //console.log("Erro Pedido!");
                                    }
                    );
                } else {
                    Ext.Msg.alert("Erro Pedido!", settings.get('minMsg'), Ext.emptyFn);
                }
           } else {
               order.set('status', 'Liberado');
               
               contSql.update (db, 'orders',
                               order.data,
                               'orderipad="' + order.data.orderipad +'"',
                               'AND',
                               function (results) {
                               //console.log("Pedido Salvo!");
                               ordersStore.load();
                               
                               //Ext.Msg.alert('Pedido Liberado!','Pedido liberado para sincronizar com o EMS', Ext.emptyFn);
                               
                               contSync.syncPedido();
                               
                               me.showOrdersForm(view, index, target, order, evt, opts);
                               },
                               function(err) {
                               //console.log("Erro Pedido!");
                               }
                               );
           }
		}
	},
	updateItem: function (view, index, target, record, evt, opts) {
           
        var orderStore = Ext.getStore('Orders'),
            app = this,
            formOrder = this.getOrdersForm();
           
        //orderStore.clearFilter(true);
           
        var status = orderStore.getById(record.data.orderipad).get('status');
           
        if (status == 'Pendente' || status == 'Errosync') {
        
            var popup = Ext.create('Ext.Panel', {
                fullscreen : true,
                centered : true,
                modal: true,
                width : 360,
                height : 440,
                layout : 'fit',            
                id: 'updatePopup',
                items : [
                    {
                        docked : 'top',
                        xtype : 'toolbar',
                        title : 'Alterar Item'
                    }, 
                    {
                        xtype : 'formpanel',
                        record: record,
                        layout: 'vbox',
                        height: 440,
                        id: 'updateForm',
                        items : [
                    {
                        xtype: 'fieldset',
                        title: record.data.itemcode,		        	
                        items: [

                            {
                                xtype : 'numberfield',
                                name : 'quantity',
                                id: 'quantityUpdate',
                                itemId: 'quantityUpdate',
                                label : 'Quantidade',
                                labelWidth: '40%',
                                listeners:{
                                    keyup: function(field, e, eOpts) {
                                        //console.log(jsNumbers(e.event));
                                        //return jsNumbers(e.event);
                                        return false;
                                    }
                                }
                            },
                            {
                                xtype : 'numberfield',
                                name : 'listPrice',
                                id: 'listPrice',
                                itemId: 'listPrice',
                                label : 'Preço Lista',
                                labelWidth: '40%',
                                disabled: true
                                
                            },
                            {
                                xtype : 'numberfield',
                                name : 'value',
                                id: 'valueUpdate',
                                itemId: 'valueUpdate',
                                label : 'Valor',
                                labelWidth: '40%',
                                listeners:{
                                    keyup: function(field, e, eOpts) {
                                        return jsNumbers(e.event);
                                    },
                                    change: function(field,newValue){
                                
                                        var desconto = (1 - (newValue / Ext.getCmp('updateForm').getValues().listPrice)) * 100;
                                
                                        if(Number(desconto))
                                            Ext.getCmp('aplDiscount').setValue(Number(desconto).toFixed(2));                                        
                                
                                        if (Number(desconto) != 0 & afterShow)
                                        	Ext.getCmp('discountUpdate').setValue(0);

                                		afterShow = true;
                                    }
                                }
                            },
                            {
                                xtype : 'numberfield',
                                name : 'discount',
                                id: 'discountUpdate',
                                itemId: 'discountUpdate',
                                label : '% Desconto',
                                labelWidth: '40%',
                                listeners:{
                                    keyup: function(field, e, eOpts) {
                                        return jsNumbers(e.event);
                                    },
                                    change: function(field,newValue){
                                        if (newValue != 0) {
                                            Ext.getCmp('valueUpdate').setValue(Ext.getCmp('updateForm').getValues().listPrice);
                                            Ext.getCmp('aplDiscount').setValue(newValue);                                            
                                        }
                                    }
                                }
                            },
                            {
                                xtype : 'numberfield',
                                name : 'aplDiscount',
                                id: 'aplDiscount',
                                itemId: 'aplDiscount',
                                label : '% Desc Aplic',
                                labelWidth: '40%',
                                disabled: true
                            },
                            {
                                xtype: 'button',
                                ui: 'gray',
                                text: 'Salvar',
                                listeners: {
                                    tap: function(btSalvar) {
                                        btSalvar.disable(true);
                                
                                        var prodStore = Ext.getStore('Products');
                                
                                        var order = orderStore.getById(record.data.orderipad);

                                        var prod = prodStore.getData().findBy(function(produto) {
                                      		return produto.get('code') == record.data.itemcode;
                                      	});
                                        
                                        itemsStore = Ext.getStore('OrderItems');

                                        formUpdate = Ext.getCmp('updateForm');
                                
                                        var valor = formUpdate.getValues().value;
                                        if (Number(valor) == 0 || !Number(valor))
                                            valor = formUpdate.getValues().listPrice;
                                
                                        var qtde = formUpdate.getValues().quantity;
                                
                                        if (qtde <= 0) {
                                            btSalvar.enable(true);
                                            Ext.Msg.alert("Quantidade Inválida!", "Quantidade do item deve ser maior que 0 (zero)", Ext.emptyFn);
                                        }
                                        else {
                                
                                            if (qtde % prod.get('loteMulti') > 0) {
                                                btSalvar.enable(true);
                                                Ext.Msg.alert("Quantidade Inválida!", "Quantidade do item deve ser multiplo de " + prod.get('loteMulti'), Ext.emptyFn);
                                            } else {

                                                record.set("value",valor);
                                                record.set("discount",formUpdate.getValues().discount);
                                                record.set("quantity",formUpdate.getValues().quantity);
                                                //record.set(formUpdate.getValues());
                                                
                                                //var contSql = App.app.getController('Sql');
                                                
                                                contSql.update (db, 'orderitems',
                                                                record.data,
                                                                'id="' + record.data.id +'"',
                                                                'AND',
                                                                function (results) {
                                                                
                                                                    itemsStore.load();
                                                                    itemsStore.data.setFilters(null);
                                                                    itemsStore.filter('orderipad', record.get('orderipad'));
                                                                    
                                                                    itemsValue = itemsStore.getData();
                                                                    
                                                                    var vlPedido = 0;
                                                                    
                                                                    itemsStore.each(function(recItem) {
                                                                                    
                                                                        var	itemValue = (Number(recItem.get('value')) * Number(recItem.get('quantity')));
                                                                         
                                                                        vlPedido = vlPedido + (itemValue - (itemValue * recItem.get('discount') / 100));
                                                                    });
                                                                
                                                                    if(order && formOrder.getValues().financialIndex > 1)
                                                                        vlPedido += (vlPedido * formOrder.getValues().financialIndex / 100);
                                                                
                                                                    order.set('value', vlPedido);
                                                                    
                                                                    contSql.update (db, 'orders',
                                                                                    order.data,
                                                                                    'orderipad="' + order.data.orderipad +'"',
                                                                                    'AND',
                                                                                    function (results) {
                                                                                        //console.log("Pedido Salvo!");
                                                                                        orderStore.load();
                                                                                        formOrder.setRecord(order);
                                                                                        
                                                                                        var createDate = Ext.getCmp('createdate');

																						createDate.setValue(Ext.Date.parse(order.data.createdate,'d/m/Y'));

																						createDate.disable();		

																						var deliveryDate = Ext.getCmp('deliverydate');	

																						deliveryDate.setValue(Ext.Date.parse(order.data.deliverydate,'d/m/Y'));
                                                                                        
                                                                                        //app.getListItemsGroups().refresh();
                                                                                    
                                                                                        Ext.getCmp('updatePopup').destroy();

                                                                                    },
                                                                                    function(err) {
                                                                                        console.log("Erro Pedido!");
                                                                                        Ext.getCmp('updatePopup').destroy();
                                                                                    }
                                                                        );
                                                                    
                                                                        
                                                                },
                                                                
                                                                function(err) {
                                                                    Ext.Msg.alert("Erro ao Salvar!", err.message, Ext.emptyFn);
                                                                    Ext.getCmp('updatePopup').destroy();
                                                                }
                                                );
                                            }
                                        }
                                    }
                                }
                            },                            
                            {
                                xtype: 'button',
                                action: 'eliminarItem',
                                ui: 'decline',
                                text: 'Eliminar',				        	
                                listeners: {
                                    tap: function () {
                                    	Ext.Msg.confirm(
											'Eliminar Item',
											'Tem certeza que deseja eliminar este item do pedido?',
											function(btn){
												if(btn == 'yes'){
													contOrder = App.app.getController('Orders');
                                        			contOrder.deletarItem(record);	                                        			
												}
												Ext.getCmp('updatePopup').destroy();

											},
											this			
										);                                        
                                    }
                                }
                            },
                            {
                                xtype: 'button',
                                action: 'cancelarUpdate',
                                ui: 'gray',
                                text: 'Cancelar',				        	
                                listeners: {
                                    tap: function () {
                                        Ext.getCmp('updatePopup').destroy();
                                    }
                                }
                            }
                        ]
                    }]
                }],
                scrollable : true,
                listeners: {
                    show: function() {

                    	afterShow = false;
                                   
                        var priceTables = Ext.getStore('PriceTables').getData().findBy(function(tables){
                            return tables.get('codeItem').trim() == record.data.itemcode.trim();
                        });
                                   
                        var priceAux = 0;
                                   
                        if (priceTables != null)
                            priceAux = Number(priceTables.get('price')).toFixed(2);
                                   
                        Ext.getCmp('listPrice').setValue(Number(priceAux).toFixed(2));
                        
                        var desconto = (1 - ( Number(record.data.value).toFixed(2) / priceAux)) * 100 ;

                        if(Number(desconto))
                            Ext.getCmp('aplDiscount').setValue(Number(desconto).toFixed(2));
                        else
                        	Ext.getCmp('aplDiscount').setValue(Number(record.data.discount).toFixed(2));


                        
                    },
                    destroy: function () {
                        //Fechar o teclado
                        Ext.Function.defer(function () {
                            document.activeElement.blur();
                        },300);
                        
                    }
                }
            });
            popup.show();
            //Ext.getCmp('quantityUpdate').focus();
            document.activeElement.blur();
        }

		//popup.showBy(view);

		
	}
});

function jsDecimals(e){
    var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;

    if(key != null)
    {
        key = parseInt(key, 10);
        if((key < 48 || key > 57) && (key < 96 || key > 105))
        {
            if(!jsIsUserFriendlyChar(key, "Decimals"))
            {
                return false;
            }
        }
        else
        {
            if(evt.shiftKey)
            {
                return false;
            }
        }
    }
    return true;
}

function jsNumbers(e)
{
        
    var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;
    
    if(key != null)
    {
        key = parseInt(key, 10);
        if((key < 48 || key > 57) && (key < 96 || key > 105))
        {
            if(!jsIsUserFriendlyChar(key, "Numbers"))
            {
                return false;
            }
        }
        else
        {
            if(evt.shiftKey)
            {
                return false;
            }
        }
    }
    return true;
}

//------------------------------------------
// Function to check for user friendly keys
//------------------------------------------
function jsIsUserFriendlyChar(val, step)
{
    // Backspace, Tab, Enter, Insert, and Delete
    if(val == 8 || val == 9 || val == 13 || val == 45 || val == 46)
    {
        return true;
    }
    // Ctrl, Alt, CapsLock, Home, End, and Arrows
    if((val > 16 && val < 21) || (val > 34 && val < 41))
    {
        return true;
    }
    if (step == "Decimals")
    {
        if(val == 188 || val == 110)
        {
            return true;
        }
    }
    // The rest
    return false;
}

function saveItem(itemPedido, rec, order, colorCorre, callback) {
    
    var itemsStore = Ext.getStore('OrderItems'),
        //contSql = App.app.getController('Sql'),
        contOrder = App.app.getController('Orders'),
        correlatosStore = Ext.getStore('Correlatos');

    
    var codeItem, qtdItem, discItem, valorItem, valorOrig;
    
    codeItem = rec.data.code;
    
    for (var i = 0; i < itemPedido.childElementCount; i++){
        
        var child = itemPedido.children[i].children[0];
                
        if (child.name != undefined){
            if (child.name == "qtdeItemPed") {
                qtdItem = child.value;
            } else {
                if (child.name == "discItemPed") {
                    discItem = child.value;
                } else {
                    valorItem = child.value
                }
            }
        }
    }
    
    valorOrig = valorItem;
        
    /*Não somar o indice financeiro no valor
    var financial = Ext.getCmp('financialIndex').getValue();
        
    if (financial != 0)
        valorItem = Number(valorItem) + Number(valorItem * financial / 100);
    */
            
    /*Não somar o desconto no valor do item
    if (Number(order.get('discount')))
        valorItem = valorItem - (Number(valorItem) * Number(order.get('discount')) / 100);
    */
    
    var itemOrder = itemsStore.getData().findBy(function(record) {
                                                return record.get('itemcode') === codeItem;
                                                });
    
    if (itemOrder) {
        
        if (qtdItem != '' && qtdItem != 0){
                        
            itemOrder.set('quantity', qtdItem);
            itemOrder.set('discount', discItem);
            itemOrder.set('value', valorItem);
            itemOrder.set('valueOrig', valorOrig)
            
            contSql.update (db, 'orderitems',
                            itemOrder.data,
                            'id="' + itemOrder.data.id +'"',
                            'AND',
                            function (results) {
                            //console.log('salva10');

                            //Ext.Msg.alert("Item Salvo!", "Alteração efetuada com sucesso!", Ext.emptyFn);

                            newValue = newValue + (qtdItem * (valorItem - (valorItem * discItem / 100)));
                                                        
                            orderIpad = itemOrder.get('orderipad');
                            callback("OK", orderIpad, newValue);
                            },
                            function(err) {

                            callback("ERROR");
                            }
			                );
        } else {
            contOrder.deletarItem(itemOrder);
            callback("OK");
        }
    } else {
        if (qtdItem != '' && qtdItem != 0){
            
            var newItem = Ext.create('App.model.OrderItems', {
                                     orderipad : order.get('orderipad'),
                                     itemcode : codeItem,
                                     description : rec.data.desc,
                                     un : rec.data.unidade,
                                     quantity : qtdItem,
                                     discount : discItem,
                                     value : valorItem,
                                     valueOrig: valorOrig
                                     });
            
            var today = new Date();
            
            newItem.set('id', newItem.get('id') + today.getMilliseconds());
            
            contSql.insert (db,'orderitems',
                          newItem.data,
                          function (results) {
                            
                          //Ext.Msg.alert("Inclusão de Itens", "Itens gravados com sucesso!", Ext.emptyFn);
                          
                          colorCorre = correlatosStore.getData().filterBy(function(recCorre){
                                                                          return recCorre.get('line') === recColor.get('lineCode');
                                                                          });
                          if (colorCorre.items.length == 0 )
                            colorCorre = null;
                            
                            
                          orderIpad = order.get('orderipad');
                            
                          //auxNewValue = newValue + (Number(newItem.get('value')) * Number(newItem.get('quantity'))),
                          //newValue = auxNewValue - (auxNewValue * newItem.get('discount') / 100);

                          newValue = newValue + (qtdItem * (valorItem - (valorItem * discItem / 100)));
                                                      
                          callback("OK", orderIpad, newValue , colorCorre);
                          },
                          function(err) {
                          callback("ERROR");
                          }
                          );
        } else {

            callback("OK");
        }
    }
}

function getItemPedido (item, rec, order, colorCorre, contCor, contItem, callback) {
    
    var success = function (results, orderIpad, value, colorCorre) {
                
        if (typeof(callback) == 'function' ) callback(results, contCor, contItem, colorCorre, orderIpad, value);
    }
    
    saveItem(item, rec, order, colorCorre, success);
    
}

function setColors (codePai, callback) {
    var productStore = Ext.getStore('Products');
    
    productStore.clearFilter(true);
    
    var colors = productStore.getData().filterBy(function(record) {
                                                 return record.get('itemPai') == codePai;
                                                 });
    
    callback("OK", colors);
}

function getCodePai (codePai, contCor, callback) {
    
    var success = function (results, colors) {
        if (typeof(callback) == 'function') callback(results, colors, contCor);
    }
    
    setColors(codePai, success);
}
