Ext.define('App.controller.SyncOrder', {
    extend: 'Ext.app.Controller',

    syncPedido: function () {

        var ordersControl = App.app.getController('Orders'),
            ordersStore = Ext.getStore('Orders'),
            itemsStore = Ext.getStore('OrderItems'),
            settings = Ext.getStore('Settings').getData().first();
       

        ordersStore.load();

        itemsStore.load();

        ordersStore.each(function (order) {

            if (order.get('status') == "Liberado") {

                //console.log('sync2');

                var jsonItems = new Array();

                //console.log(itemsStore.getData());

                var filters = itemsStore.getFilters();

                itemsStore.clearFilter(true);

                //console.log(itemsStore.getCount());

                var paymentTermsStore = Ext.getStore('PaymentTerms'),
                    financialValue = 0;

                var condPagto = paymentTermsStore.getData().findBy(function (rec) {
                    return rec.data.code == order.get('paymentTerms');
                });

                itemsStore.each(function (items) {
                    if (items.data.orderipad == order.data.orderipad) {
                        jsonItems.push({
                            orderipad: items.get('orderipad'),
                            itemcode: items.get('itemcode'),
                            un: items.get('un'),
                            quantity: items.get('quantity'),
                            discount: items.get('discount'),
                            value: items.get('value')
                        });
                    }
                });

                itemsStore.setFilters(filters);

                Ext.Ajax.request({
                    url: urlws + "saveOrder.p",
                    method: 'post',
                    disableCaching: true,
                    useDefaultXhrHeader: false,
                    timeout: 300000,
                    params: {
                        paymentTerms: order.get('paymentTerms'),
                        custnum: order.get('custnum'),
                        ordercust: order.get('ordercust'),
                        orderipad: order.get('orderipad'),
                        createdate: order.get('createdate'),
                        siteid: order.get('siteid'),
                        deliverydate: order.get('deliverydate'),
                        discount: order.get('discount'),
                        discountlist: order.get('discountlist'),
                        ordertype: order.get('ordertype'),
                        remarks: order.get('remarks'),
                        carrier: order.get('carrier'),
                        username: settings.get('username'),
                        checkOrder: "false",
                        items: Ext.util.JSON.encode(jsonItems)
                    },
                    success: function (response) {
                        try {
                            var loginResponse = Ext.JSON.decode(response.responseText);
                        } catch (err) {
                            console.log(err.message);
                            //me.signInFailure(err.message);
                            return;
                        }

                        console.log('response - ' + loginResponse.success);

                        if (loginResponse.success === "true") {

                            var orderSql = new Array();

                            orderSql.push({
                                status: 'Integrado',
                                deliverydate: loginResponse.deliverydate,
                                ordernum: loginResponse.ordernum,
                                ordercust: loginResponse.ordercust,
                                statusEMS: loginResponse.statusEMS,
                                valueEMS: Number(loginResponse.value).toFixed(2)
                            });

                            order.set('status', 'Integrado');


                            var d = new Date();

                            d = d.getTime() + 1 * 1000; //60 seconds from now
                            d = new Date(d);

/*
                            window.addNotification({
                                date: d,
                                //repeat:'daily',
                                alertBody: 'Cliente - ' + order.get('custname') + '\n' +
                                    'Pedido - ' + loginResponse.ordercust + '\n' +
                                    'Pedido incluído com sucesso!',
                                hasAction: true,
                                badge: 1,
                                notificationId: loginResponse.orderipad,
                                soundName: 'beep.caf',
                                background: 'app.background',
                                foreground: 'app.running'
                            });
                            */

                            contSql.query(db,
                                'UPDATE orders SET status = "Integrado", deliverydate = "' + loginResponse.deliverydate + '", ordernum = "' + loginResponse.ordernum + '", ordercust = "' + loginResponse.ordercust + '", statusEMS = "' + loginResponse.statusEMS + '", valueEMS = "' + Number(loginResponse.value).toFixed(2) + '" WHERE orderipad="' + loginResponse.orderipad + '"',
                                function (results) {
                                    //console.log("Pedido Salvo!");
                                    ordersStore.load();
                                    Ext.getCmp('orderslist').refresh();
                                    Ext.getCmp('customerslist').refresh();
                                    //console.log('loadorders');
                                },
                                function (err) {
                                    //console.log("Erro Pedido!");
                                }
                            );
                        } else {

                            order.set('status', 'Errosync');

                            var d = new Date();

                            //console.log(order);

                            d = d.getTime() + 1 * 1000; //60 seconds from now
                            d = new Date(d);

/*
                            window.addNotification({
                                date: d,
                                //repeat:'daily',
                                alertBody: 'Cliente - ' + order.get('custname') + '\n' +
                                    'Pedido - ' + order.get('ordercust') + '\n' +
                                    'Erro - ' + loginResponse.message,
                                hasAction: true,
                                badge: 1,
                                notificationId: loginResponse.orderipad,
                                soundName: 'beep.caf',
                                background: 'app.background',
                                foreground: 'app.running'
                            });
                            */


                            //console.log('sync4');

                            contSql.query(db,
                                'UPDATE orders SET status = "Errosync" WHERE orderipad="' + loginResponse.orderipad + '"',
                                function (results) {
                                    //console.log("Pedido Salvo!");
                                    ordersStore.load();
                                    Ext.getCmp('orderslist').refresh();
                                    Ext.getCmp('customerslist').refresh();
                                    //console.log('loadorders');
                                },
                                function (err) {
                                    //console.log("Erro Pedido!");
                                }
                            );
                        }
                    },
                    failure: function (a, b, c) {
                        console.log("falhou: ");
                        console.log(a);

                        Ext.Ajax.request({
                            url: urlws + "saveOrder.p",
                            method: 'post',
                            disableCaching: true,
                            useDefaultXhrHeader: false,
                            timeout: 300000,
                            params: {
                                checkOrder: "true",
                                username: settings.get('username'),
                                custnum: order.get('custnum'),
                                ordercust: order.get('ordercust'),
                                orderipad: order.get('orderipad'),
                                createdate: order.get('createdate')
                            },
                            success: function (response) {
                                try {
                                    var loginResponse = Ext.JSON.decode(response.responseText);
                                } catch (err) {
                                    console.log(err.message);
                                    //me.signInFailure(err.message);
                                    return;
                                }

                                console.log('response - ' + loginResponse.success);

                                if (loginResponse.success === "true") {

                                    var orderSql = new Array();

                                    orderSql.push({
                                        status: 'Integrado',
                                        deliverydate: loginResponse.deliverydate,
                                        ordernum: loginResponse.ordernum,
                                        ordercust: loginResponse.ordercust,
                                        statusEMS: loginResponse.statusEMS,
                                        valueEMS: Number(loginResponse.value).toFixed(2)
                                    });

                                    order.set('status', 'Integrado');


                                    var d = new Date();

                                    d = d.getTime() + 1 * 1000; //60 seconds from now
                                    d = new Date(d);

/*
                                    window.addNotification({
                                        date: d,
                                        //repeat:'daily',
                                        alertBody: 'Cliente - ' + order.get('custname') + '\n' +
                                            'Pedido - ' + loginResponse.ordercust + '\n' +
                                            'Pedido incluído com sucesso!',
                                        hasAction: true,
                                        badge: 1,
                                        notificationId: loginResponse.orderipad,
                                        soundName: 'beep.caf',
                                        background: 'app.background',
                                        foreground: 'app.running'
                                    });
                                    */

                                    contSql.query(db,
                                        'UPDATE orders SET status = "Integrado", deliverydate = "' + loginResponse.deliverydate + '", ordernum = "' + loginResponse.ordernum + '", ordercust = "' + loginResponse.ordercust + '", statusEMS = "' + loginResponse.statusEMS + '", valueEMS = "' + Number(loginResponse.value).toFixed(2) + '" WHERE orderipad="' + loginResponse.orderipad + '"',
                                        function (results) {
                                            //console.log("Pedido Salvo!");
                                            
                                            ordersStore.load();
                                            Ext.getCmp('orderslist').refresh();
                                            Ext.getCmp('customerslist').refresh();
                                            //console.log('loadorders');
                                        },
                                        function (err) {
                                            //console.log("Erro Pedido!");
                                        }
                                    );
                                } else {

                                    order.set('status', 'Errosync');

                                    var d = new Date();

                                    d = d.getTime() + 1 * 1000; //60 seconds from now
                                    d = new Date(d);
                                    
                                    /*

                                    window.addNotification({
                                        date: d,
                                        //repeat:'daily',
                                        alertBody: 'Cliente - ' + order.get('custname') + '\n' +
                                            'Pedido - ' + order.get('ordercust') + '\n' +
                                            'Erro - ' + loginResponse.message,
                                        hasAction: true,
                                        badge: 1,
                                        notificationId: loginResponse.orderipad,
                                        soundName: 'beep.caf',
                                        background: 'app.background',
                                        foreground: 'app.running'
                                    });
                                    */

                                    contSql.query(db,
                                        'UPDATE orders SET status = "Errosync" WHERE orderipad="' + loginResponse.orderipad + '"',
                                        function (results) {
                                            //console.log("Pedido Salvo!");
                                            ordersStore.load();
                                            Ext.getCmp('orderslist').refresh();
                                            Ext.getCmp('customerslist').refresh();
                                            //console.log('loadorders');
                                        },
                                        function (err) {
                                            //console.log("Erro Pedido!");
                                        }
                                    );
                                }
                            },
                            failure: function (a, b, c) {
                                console.log("falhou2: ");
                                console.log(a);
                            }
                        });
                    }
                });
            }
        });
    }
});
