// JavaScript Document
/**
 * @author Cam
 * @version 1.0
 *
 * SQL Framework for a Sencha Touch & Phonegap Mobile application
 */

/**
 * app.utils()
 * Utility functions controller
 */


 Ext.define('App.controller.Sql', {
	extend: 'Ext.app.Controller',

//app.sql = new Ext.Controller({
	/**
	 * app.sql.build();
	 * Builds the local database architecture if none
	 */
	build: function () { 

		//App.app.utils.setStatus('Initializing...');
		
		function dbCreate(DB) {

            DB.executeSql('CREATE TABLE IF NOT EXISTS customers (bairro TEXT, carrier TEXT, ordertype TEXT, paymentTerm TEXT, id INTEGER PRIMARY KEY, code TEXT, name TEXT, cnpj TEXT, abrev_name TEXT, address TEXT, city TEXT, uf TEXT, email TEXT)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS orderitems (valueOrig TEXT, id TEXT, orderipad TEXT, itemcode TEXT, description TEXT, un TEXT, quantity NUMERIC, discount NUMERIC, value NUMERIC)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS orders (financialIndex NUMERIC, valueEMS NUMERIC, paymentTerms TEXT, ordercust TEXT, custname TEXT, custnum TEXT, orderipad INTEGER, ordernum TEXT, createdate TEXT, siteid TEXT, deliverydate TEXT, discount NUMERIC, discountlist NUMERIC,ordertype TEXT, remarks TEXT, value NUMERIC, status TEXT, statusEMS TEXT, carrier TEXT)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS paymentterms (id INTEGER PRIMARY KEY, code TEXT, desc TEXT, financialIndex NUMERIC)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS ordertype (id INTEGER PRIMARY KEY, code TEXT, desc TEXT)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS pricetables (id TEXT, code TEXT, codeItem TEXT, un TEXT, price NUMERIC)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS settings (urlDownload TEXT, lastAcc TEXT, minMsg TEXT, minValue NUMERIC, id INTEGER PRIMARY KEY, bloqIpad TEXT, sitelist TEXT, lastorder NUMERIC, urlws TEXT, username TEXT, password TEXT,diasSemPedido INTEGER DEFAULT 90)');
			DB.executeSql('CREATE TABLE IF NOT EXISTS support (filename TEXT, filetype TEXT, description TEXT, uptodate TEXT, docDate TEXT)');
            
            DB.executeSql('CREATE TABLE IF NOT EXISTS customerpaymentterms (id TEXT, custnum TEXT, paymentTerm TEXT, financialIndex NUMERIC)');
            DB.executeSql('CREATE TABLE IF NOT EXISTS carriers (code TEXT, abrevName TEXT, fullName TEXT)');
            
            /*
            DB.executeSql('CREATE INDEX idx_customer_cnpj ON customers(cnpj ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_customer_code ON customers(code ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_customer_id ON customers(id ASC)');
            DB.executeSql('CREATE INDEX idx_customer_name ON customers(name ASC)');
            DB.executeSql('CREATE INDEX idx_item_code ON orderitems(itemcode ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_item_id ON orderitems(id ASC)');
            DB.executeSql('CREATE INDEX idx_item_order ON orderitems(orderipad ASC)');
            DB.executeSql('CREATE INDEX idx_order_custnum ON orders(custnum ASC)');
            DB.executeSql('CREATE INDEX idx_order_date ON orders(createdate ASC)');
            DB.executeSql('CREATE INDEX idx_order_ordercust ON orders(ordercust ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_order_orderipad ON orders(orderipad ASC)');
            DB.executeSql('CREATE INDEX idx_order_ordernum ON orders(ordernum ASC)');
            DB.executeSql('CREATE INDEX idx_order_status ON orders(status ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_ordertype_code ON ordertype(code ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_pay_code ON paymentterms(code ASC)');
            DB.executeSql('CREATE UNIQUE INDEX idx_price_id ON pricetables(id ASC)');
            DB.executeSql('CREATE INDEX idx_price_item ON pricetables(codeItem ASC)');*/
		}
		
		function dbCreateError(err) {
            console.log(err);
		}
		
		function dbCreateSuccess() {

		}

		db.transaction(dbCreate, dbCreateError, dbCreateSuccess);
	},
	
	/**
	 * app.sql.getVar(column, table, where);
	 * Gets a single variable from the specified table
	 * @param column:String The field you're requestiong
	 * @param table:String The table to search
	 * @param where:Object Name value pair of columns to match agains
	 * @param compare:String [OR|AND] Whow to compare the where param
	 * @param callback:Function The function to run after a successful query
	 * @param error:Function The function to run after an unsuccessful query
	 */
	getVar: function (column, table, where, compare, callback, error) {
		if (!db) { return; }
		if (!column || !table || !where) { return; }
		if (!callback) { callback = Ext.emptyFn; }
		if (!error) { error = Ext.emptyFn; }
		if (compare != 'OR' && compare != 'AND') { compare = 'AND'; }
		var arr = [];
		for (var p in where) { 
			var v = where[p];
			var s = p + " = '" + v + "'";
			arr.push(s); 
		}
		if (arr.length > 0) { var str = arr.join(' '+compare+' '); }
		if (!str) { return; }
		
		var sql = "SELECT " + column + " FROM " + table + " WHERE " + str + " LIMIT 1";
		
		function sqlSuccess(DB, results) {
			var output;
			if (results.rows.length > 0) { 
				var obj = results.rows.item(0);
				output = obj[column]; 
			}
			callback(output);
		}
		
		function sqlError(err) {
			if (err.code) { error(err); }
		}
		
		function sqlInit(DB) {
			DB.executeSql(sql, [], sqlSuccess, sqlError);
		}
		
		db.transaction(sqlInit, sqlError);
	},
	 
	 /**
	  * app.sql.insert(table, data, callback, error);
	  * Inserts an item into the specified table
	  * @param table:String the table to insert row
	  * @param data:Object Name value pair of columns and values
	  * @param callback:Function The function to run after a successful query
	  * @param error:Function The function to run after an unsuccessful query
	  */
	insert: function (db, table, data, callback, error) {
        		
		if (!db) { return; }
		if (!data) { return; }
		if (!callback) { callback = Ext.emptyFn; }
		if (!error) { error = Ext.emptyFn; }
		
		var ins = {flds: [], vals: []};
		for (var p in data) { 
			var prop = p;
			var val = data[p];
			ins.flds.push(prop);
			ins.vals.push(val);
		}
		
		var sql = "INSERT INTO "+table+" ("+ins.flds.join(",")+") VALUES ('"+ins.vals.join("','") + "')";
						
		function sqlSuccess(DB, results) {
			callback(results); 
		}
		
		function sqlError(trans, err) {
			if (err.code) { error(err); }
            console.log(sql);
            console.log(err);
		}
        
        function sqlInit(DB) {
			DB.executeSql(sql, [], sqlSuccess, sqlError);
		}	
            //console.log(sql);
            
		db.transaction(sqlInit, sqlError); 
	},
	
	/**
	 * app.sql.query(sql);
	 * Executes an SQL query
	 * @param sql:String the sql query to perform
	 */
	query: function (db, sql, callback, error) {

		//var db = openDatabase("ipadrev", "1.0", "Revenda Sayerlack", 1024 * 1024 * 5); 

		if (!db) { return; }
		if (!sql) { return; }
		if (!callback) { callback = Ext.emptyFn; }
		if (!error) { error = Ext.emptyFn; }
		
		function sqlSuccess(DB, results) { 
			callback(results);
		}
		
		function sqlError(err) { 			
			if (err.code) { error(err); }
		}
		
		function sqlInit(DB) {		
			DB.executeSql(sql, [], sqlSuccess, sqlError);	
		}

		db.transaction(sqlInit, sqlError);			
	},	 
	 
	update: function (db, table, data, where, compare, callback, error) { 
		if (!db) { return; }
		if (!data) { return; }
		if (!callback) { callback = Ext.emptyFn; }
		if (!error) { error = Ext.emptyFn; }
		if (compare != 'OR' && compare != 'AND') { compare = 'AND'; }
		
		compare = compare.toUpperCase();
		
		var p;
		
		// Build the where clause
		var whr = [];

		whr.push(where);


		/*for (p in where) { 
			var v = where[p];
			var s = p + " = '" + v + "'";
			whr.push(s); 
		}		*/
		
		// Build the update statement
		var upd = [];
		for (p in data) { 
			var prop = p;
			var val = data[p];
			var str = prop + " = " + "'"+val+"'";
			upd.push(str);
		}
		
		// Build the full sql statement
		var sql = "UPDATE "+table+" SET "+upd.join(', ')+" WHERE "+ whr.join(' '+compare+' ');
            
        //console.log(sql);
            
		function sqlSuccess(DB, results) { 			
			callback(results); 
		}
		
		function sqlError(err) {
            console.log("Erro ao atualizar: " + err.code);
			if (err.code) { error(err); }
		}
		
		function sqlInit(DB) {
			DB.executeSql(sql, [], sqlSuccess, sqlError);
		}
		
		db.transaction(sqlInit, sqlError); 
	}
	  
});
