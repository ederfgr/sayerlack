Ext.define('App.controller.Customers', {
	extend: 'Ext.app.Controller',

	config: {
		refs: {
			customersContainer: 'customersContainer',
			customersList: {
				main: 'customersContainer',
				selector: 'customerslist'
			},
			mainPage: {
                xtype: 'main',
                selector: 'main',
                autoCreate: true
            },
			customersForm: {
				main: 'customersContainer',
				xtype: 'customersform',
				selector: 'customersform',
				autoCreate: true
			},
			orders: 'customersContainer customersForm list',
			toolbarCustomersForm: '#toolbarCustomersForm',
			listCustomersForm: 'listCustomersForm'
		},

		control: {
			'customerslist searchfield[itemId=customer_search]' : {
	            clearicontap :  'onSearchClearIconTap',
	            keyup: 'onSearchKeyUp'
	        },
	        customersList: {
				itemtap: 'showCustomersForm'
			},
			listCustomersForm: {
				itemtap: 'tapCustomersForm'
			},
			'button[action=voltarCustomersList]' : {
				tap: 'showCustomersList'
			},
			'button[action=novoPedido]' : {
				tap: 'addPedido'
			}
		}
	},

	onSearchKeyUp: function(field) {  
        //get the store and the value of the field  
        
        var value = field.getValue(),  
        store = Ext.getCmp('customerslist').getStore();
  
        //first clear any current filters on thes tore  
        store.clearFilter();
  
        //check if a value is set first, as if it isnt we dont have to do anything  
        if (value) {  
            //the user could have entered spaces, so we must split them so we can loop through them all  
            var searches = value.split(' '),
            regexps = [],  
            i;  
  
            //loop them all  
            for (i = 0; i < searches.length; i++) {  
                //if it is nothing, continue  
                if (!searches[i]) continue;  
  
                //if found, create a new regular expression which is case insenstive  
                regexps.push(new RegExp(searches[i], 'i'));
            }  
  
            //now filter the store by passing a method  
            //the passed method will be called for each record in the store  
            store.filter(function(record) 
            {  
                var matched = [];
                
                value = value.toUpperCase();
        
                didMatch = record.get('code').indexOf(value) != -1 || record.get('abrev_name').indexOf(value) != -1 || record.get('name').indexOf(value) != -1 || record.get('cnpj').indexOf(value) != -1 || record.get('city').indexOf(value) != -1;
                 
                matched.push(didMatch);
                if (didMatch)
                    return matched[0];
        
                //loop through each of the regular expressions  
                for (i = 0; i < regexps.length; i++)
                {  
                    var  search = regexps[i],  
            		   didMatch = record.get('code').match(search) || record.get('abrev_name').match(search) || record.get('name').match(search) || record.get('cnpj').match(search) || record.get('city').match(search);  //if it matched the first or last name, push it into the matches array
  
                    matched.push(didMatch);
            	}  //if nothing was found, return false (dont so in the store)                 
  
	            if (regexps.length > 1 && matched.indexOf(false) != -1) 
	            {
                    return false;
	            } 
	            else
	            {  
	            	//else true true (show in the store)  
	                return matched[0];  
	            }  
            });  
        }  
    },  
  
    /** 
     * Called when the user taps on the clear icon in the search field. 
     * It simply removes the filter form the store 
     */  
    onSearchClearIconTap: function() {  
        //call the clearFilter method on the store instance  
        Ext.getCmp('customerslist').getStore().clearFilter();  
    },

    showCustomersList: function(){

    	custnum = 0;

    	Ext.getStore('Orders').clearFilter();
           
    	this.getCustomersContainer().setActiveItem(this.getCustomersList());
	},

    showCustomersForm: function(view, index, target, record, evt, opts)
    {

    	var ordersStore = Ext.getStore('Orders');
           
    	ordersStore.clearFilter();

    	custnum = record.get('code');

		ordersStore.filterBy(function(rec) {
		    return rec.get('custnum') === custnum;
		});

		if(record && record.data)
		{
			this.getCustomersForm().setRecord(record);
			this.getToolbarCustomersForm().setTitle(record.get('code') + ' - ' +record.get('name'));
		}

		this.getCustomersContainer().setActiveItem(this.getCustomersForm(), { type: 'slide', direction: 'left' });
	},

	addPedido: function()
	{

		this.getMainPage().setActiveItem(1);
		
		this.getApplication().getController('Orders').showOrdersForm();
	}
});

function tapListCustomersForm(view, index, target, record, evt, opts) {

	var app = this.App.app;

	var ordersController = app.getController('Orders');

	var mainController = app.getController('Main');

	mainController.getMainPage().setActiveItem(1);

	ordersController.showOrdersForm(view, index, target, record, evt, opts);
}